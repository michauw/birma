import itertools
import re
import os
import logging

def loadFile (path):

    with open (path) as fin:
        return fin.readlines ()

def loadLadder (path):

    return [tuple ([int (el) for el in line.split ('\t')[:2]]) for line in loadFile (path)]

def pairwise(iterable):        # get
    a, b = itertools.tee(iterable)
    next (b)
    return itertools.izip(a, b)

def getNext (pairs, ps, drc):
    while True:
        pair = pairs.next ()
        if pair[0][:2] not in ps:
            return pair[1][drc] - pair[0][drc]

def countLines (path):
    try:
        with open (path) as fin:
            return len (fin.readlines ())
    except IOError:
        return 0

def addAlignmentTags_old (pathin, lang1, lang2, ladd, ps, drc, pathout = ''):

    '''Add tags <Align_LANG1_LANG2> to a CWB file'''

    res = []
    pairs = pairwise (ladd)
    tagOp = '<Align_%s_%s>' % (lang1, lang2)
    tagCl = '</' + tagOp[1:]
    with open (pathin) as fin:
        go = getNext (pairs, ps, drc)
        sId = 0
        opn = False
        for line in fin:
            if line == '<s>\n' or line.startswith ('<s '):
                try:
                    while not go:
                        res.append ('%s\n--\n%s' % (tagOp, tagCl))
                        go = getNext (pairs, ps, drc)
                except StopIteration:
                    break
                if not opn:
                    res.append (tagOp)
                    opn = True
                res.append ('<s id="%d">' % sId)
                sId += 1
                go -= 1
            elif line == '</s>\n':
                res.append ('</s>')
                if not go:
                    if opn:
                        res.append (tagCl)
                        opn = False
                    try:
                        go = getNext (pairs, ps, drc)
                    except StopIteration:
                        break
            else:
                res.append (line.strip ())
    if not pathout:        # if output path not provided, overwrite input file
        pathout = pathin
    with open (pathout, 'w') as fout:
        fout.write ('\n'.join (res))

def addAlignmentTagsSingle (path_lang1, path_lang2, cases, pathalign, lang1, lang2, drc, pathout = ''):

    '''Add tags <Align_LANG1_LANG2> to a CWB file'''

    pairs = None
    tagOp = '<Align_%s_%s %s>' % (lang1, lang2, '%d')
    tagCl = '</%s>' % tagOp[1:-3]
    sId = 0
    alg = 0
    if not pathout:        # if output path not provided, overwrite input file
        pathout = pathin
    fres = open (pathout, 'w')
    if drc:
        tmp = path_lang1
        path_lang1 = path_lang2
        path_lang2 = tmp
    for case_no in cases:
        pathin = os.path.join (path_lang1, case_no + '.vert')
        if not os.path.exists (pathin):
            logging.warning ('not found: %s', case_no)
            for i in range (countLines (os.path.join (path_lang2, case_no + '.sent'))):
                fres.write ('%s\n--\n%s\n' % (tagOp % alg, tagCl))
                alg += 1
            continue
        with open (pathin) as fin:
            try:
                ladder = loadLadder (os.path.join (pathalign, case_no + '.hun'))
                pairs = pairwise (ladder)
            except IOError:
                if os.path.exists (os.path.join (path_lang2, case_no + '.sent')):   # hunalign error
                    logging.error ('!! Hunalign error: %s', case_no)
                    continue
                pairs = None
            go = 0
            for line in fin:
                if line == '<s>\n' or line.startswith ('<s '):
                    # print ('s id:', sId)
                    try:
                        while not go:
                            go = getNext (pairs, [], drc)
                            if not go:
                                fres.write ('%s\n--\n%s\n' % (tagOp % alg, tagCl))
                                alg += 1
                    except StopIteration:
                        logging.error ('getNext stop iteration - that should not happen!')
                    except AttributeError:
                        go = 1
                            # print ('s0 id:', sId + 1,)
                    # except AttributeError:  #there's no alignment for that case
                    #     res.append (tagOp % alg)
                    #     alg += 1
                    #     opn = True
                    if go == 1:
                        fres.write (tagOp % alg + '\n')
                        alg += 1
                    fres.write ('<s id="%d">\n' % sId)
                    sId += 1
                elif line == '</s>\n':
                    fres.write ('</s>\n')
                    if go == 1:
                        fres.write (tagCl + '\n')
                    go -= 1
                else:
                    fres.write (line)
            try:
                while True:
                    go = getNext (pairs, [], drc)
                    fres.write ('%s\n--\n%s\n' % (tagOp % alg, tagCl))
                    alg += 1
            except StopIteration:
                pass
            except AttributeError:
                pass
    fres.close ()

def caseInAll (case, path, langs):

    '''Check if for all pairs of languages alignment exists'''

    for l1, l2 in itertools.combinations (langs, 2):
        exists1 = os.path.exists (os.path.join (path, l1, case + '.sent'))
        exists2 = os.path.exists (os.path.join (path, l2, case + '.sent'))
        exists_alg = os.path.exists (os.path.join (path, l1 + '-' + l2, case + '.hun'))
        if exists1 and exists2 and not exists_alg:
            logging.warning ('Both sent files exist but no hun file! Case: %s, lang1: %s, lang2: %s, len1: %d, len2: %d', case, l1, l2, countLines (os.path.join (path, l1, case + '.sent')), countLines (os.path.join (path, l2, case + '.sent')))
            return False

    return True

def getCases (path, langs):

    '''Find all the cases numbers for each language along with the number of
    sentences'''

    cases = set ()
    langs.sort ()
    for lang in langs:
        for fn in [f for f in os.listdir (os.path.join (path, lang)) if f.endswith ('.sent')]:
            cases.add (fn[:-5])
    # remove cases, which don't have alignment for all pairs of languages
    cases = [case for case in cases if caseInAll (case, path, langs)]

    # for each case add the information about number of sentences in each language
    cases_updated = []
    for case in cases:
        cases_updated.append ((case, {}))
        for lang in langs:
            cases_updated[-1][1][lang] = countLines (os.path.join (path, lang, case + '.sent'))

    return cases_updated

def detectBrokenCases (path, langs, remove = False):

    # There are rare cases in which certain file has been abridged and contains
    # only small part of sentences. Because of huge difference between sentence
    # numbers, hunalign can't handle such cases and it skips them

    '''Detect (and remove) all cases that are incomplete and cannot be aligned'''

    langs.sort ()
    broken = []
    for l1, l2 in itertools.combinations (langs, 2):
        cases1 = [os.path.splitext (fname)[0] for fname in os.listdir (os.path.join (path, l1))
                  if fname.endswith ('.sent')]
        cases2 = [os.path.splitext (fname)[0] for fname in os.listdir (os.path.join (path, l2))
                  if fname.endswith ('.sent')]
        for case in cases1:
            if not case in cases2:
                continue    # there's no case counterpart in lang2, so there's no alignment
            if not os.path.exists (os.path.join (path, l1 + '-' + l2, case + '.hun')):
                count1 = countLines (os.path.join (path, l1, case + '.sent'))
                count2 = countLines (os.path.join (path, l2, case + '.sent'))
                broken.append ([case, (l1, count1), (l2, count2)])
    for brk in broken:
        logging.warning ('Broken case: %s; lang1: %s, %d; lang2: %s, %d', brk[0], brk[1][0], brk[1][1], brk[2][0], brk[2][1])
        if brk[1][1] < brk[2][1]:   # the number of sentences in lang1 is smaller
            which_lang = brk[1][0]
        else:
            which_lang = brk[2][0]
        to_remove = os.path.join (path, which_lang, brk[0] + '.sent')
        if remove:
            logging.info ('Removing file: %s', to_remove)
            os.remove (to_remove)
        else:
            logging.info ('File to remove: %s', to_remove)

def addAlignmentTagsMulti (path, langs = [], outdir = '', partial = []):

    '''Add tags <Align_LANG1_LANG2> to a CWB file for all language pairs'''

    if not outdir:
        outdir = 'align'
    if not os.path.exists (os.path.join (path, outdir)):
        os.makedirs (os.path.join (path, outdir))
    if not langs:
        # get all language names basing on the filenames in the given directory
        langs = [os.path.splitext (fn)[0] for fn in os.listdir (path_lang)
                 if fn.endswith ('.vert')]
    # print ('get cases...  ',)
    cases = getCases (path, langs)
    print ('ok, cases:', len (cases))
    lang_state = {}
    s_ids = dict ([(lang, [1, 0, 1, 0]) for lang in langs])
    tag_open = '<Align_%s_%s>'
    tag_close = '</Align_%s_%s>'
    completed = 0

    # dict that holds all the open input and output files
    if not partial:
        files = dict ([(lang, (open (os.path.join (path, outdir, lang + '_alg.cwb'), 'w'))) for lang in langs])
    else:
        files = dict ([(lang, (open (os.path.join (path, outdir, lang + '_alg.cwb'), 'w'))) for lang in partial])
    for f in files:
        print (f, files[f].name)

    for number, case in enumerate (cases):

        percent = int (float (number) / len (cases) * 100)
        if percent > completed:
            print ('completed: %d%%' % percent)
            completed = percent
            # if not percent % 10:
            for f in files:
                files[f].flush ()
        for lang in langs:
            s_ids[lang][0] = 1
            s_ids[lang][0] = 0
            # print ('lang:', lang)
            if partial and not lang in partial:
                continue
            pathin = os.path.join (path, lang, case[0] + '.vert')
            aligned_langs = [l for l in langs if l != lang]
            fres = files[lang]
            # if the case doesn't exist for certain language, make artificial sentences, aligned to other languages
            if not os.path.exists (pathin):
                # print ('not found:', lang, case[0])
                for i in range (max (case[1].values ())):
                    artificial_sentence = []
                    for j in range (len (aligned_langs)):
                        if i < case[1][aligned_langs[j]]: # check if case in aligned language has that many sentences
                            pair = tuple (sorted ([lang, aligned_langs[j]]))
                            artificial_sentence.insert (j, tag_open % pair)
                            # artificial_sentence.insert (len (artificial_sentence) - j, tag_close % pair)
                    artificial_sentence.append ('--')
                    for tag in reversed (artificial_sentence[:-1]):
                        artificial_sentence.append ('</' + tag[1:])
                    fres.write ('\n'.join (artificial_sentence) + '\n')
                continue    # job for that lang and case is done, take another one
            with open (pathin) as fin:
                for aligned in [l for l in langs if l != lang]:
                    lang_state[aligned] = {'paired': None, 'sents': 0, 'open': False}
                    pair = tuple (sorted ([lang, aligned]))
                    try:
                        pathalign = os.path.join (path, '%s-%s' % pair, case[0] + '.hun')
                        ladder = loadLadder (pathalign)
                        paired = pairwise (ladder)  # indexes of sentences in aligned languages
                        lang_state[aligned] = {'paired': paired, 'sents': 0, 'open': False}
                    except IOError:
                        if os.path.exists (os.path.join (path, aligned, case[0] + '.sent')):   # hunalign error
                            logging.error ('!! Hunalign error: %s %s %s', case, lang, aligned)
                            raise
                            continue
                        lang_state[aligned] = {'paired': None, 'sents': 0, 'open': False}
                prev = []
                for line in fin:
                    if line == '<s>\n' or '<s ' in line:
                        for aligned in aligned_langs:
                            pair = tuple (sorted ([lang, aligned]))
                            drc = int (pair[1] == lang)
                            try:
                                while not lang_state[aligned]['sents']:
                                    # if s_ids[lang] < 7 and lang == 'DEU' and aligned == 'FRA':
                                    #     print ('getNext:', s_ids[lang])
                                    lang_state[aligned]['sents'] = getNext (lang_state[aligned]['paired'], [], drc)
                                    if not lang_state[aligned]['sents']:
                                        fres.write ('%s\n--\n%s\n' % (tag_open % pair, tag_close % pair))
                            except StopIteration:
                                logging.error ('StopIteration error: %s %s %s %d', lang, aligned, case, s_ids[lang])
                                #print (''.join (prev))
                                #raise
                            except AttributeError:
                                lang_state[aligned]['sents'] = 1
                        for aligned in aligned_langs:
                            pair = tuple (sorted ([lang, aligned]))
                            if lang_state[aligned]['sents'] > 0 and not lang_state[aligned]['open']:
                                fres.write (tag_open % pair + '\n')
                                lang_state[aligned]['open'] = True
                        # fres.write ('<s id="%d;%d;%d;%d">\n' % tuple (s_ids[lang]))
                        fres.write (line)
                        s_ids[lang][0] += 1
                        s_ids[lang][2] += 1
                    elif line == '</s>\n':
                        fres.write ('</s>\n')
                        for aligned in reversed (aligned_langs):
                            pair = tuple (sorted ([lang, aligned]))
                            if lang_state[aligned]['sents'] == 1:
                                fres.write (tag_close % pair + '\n')
                                lang_state[aligned]['open'] = False
                            lang_state[aligned]['sents'] -= 1
                    elif line == '<p>\n' or line == '<p/>\n' or '<p ' in line:
                        s_ids[lang][1] += 1
                        s_ids[lang][3] += 1
                        fres.write (line)
                    elif line == '<g/>\n':
                        pass
                    else:
                        fres.write (line)
                    prev.append (line)
                remaining = []
                while True:
                    for aligned in aligned_langs:
                        pair = tuple (sorted ([lang, aligned]))
                        drc = int (pair[1] == lang)
                        try:
                            getNext (lang_state[aligned]['paired'], [], drc)
                            remaining.append (tag_open % pair)
                        except (StopIteration, AttributeError):
                            pass
                    if not remaining:
                        break
                    remaining.append ('--')
                    for tag in reversed (remaining[:-1]):
                        remaining.append ('</' + tag[1:])
                    try:
                        fres.write ('\n'.join (remaining) + '\n')
                        remaining = []
                    except:
                        print (lang, len (remaining))
    for fres in files.values ():
        print 'closing:', fres.name
        fres.close ()
        print fres.name + ':', fres.closed


def makePs (plang1, plang2, phun):

    lang1 = loadFile (plang1)
    lang2 = loadFile (plang2)
    alignment = loadFile (phun)

    ps = set ()
    for line in alignment:
        l, r = line.strip ().split ('\t')[:2]
        l = int (l)
        r = int (r)
        if lang1[l] == '<p>\n' or lang2[r] == '<p>\n':
            ps.add ((l, r))

    return ps

def makeLine (plang1, plang2):

    allCases = []
    for fname in (os.listdir (plang1) + os.listdir (plang2)):
        allCases += os.path.splitext (fname)[0]

    return sorted (set (allCases))

def adjustFile (path, pathout = ''):

    tags = ['case_name', 'case_number', 'date', 'doc_cellar']
    pattern = r'</?(?:[pg][ />]|doc)'
    remove = ['</?g/?>', '</?p[ />]', '</?doc']
    if not pathout:
        pathout = path + '.adjusted'

    with open (path) as fin, open (pathout, 'w') as fout:
        meta = []
        skip = False
        ids = [0, 0, 0, 0]
        for ind, line in enumerate (fin):
            if ind and not ind % 1000000:
                print '.',
            if line[0] != '<':
                if line[0] != '-':
                    try:
                        word, pos, lemma = line.strip ().split ('\t')[:3]
                        if lemma[-2] == '-':
                            line = '\t'.join ([word, pos, lemma[:-2]]) + '\n'
                    except ValueError:
                        pass
            elif line == '<g/>\n' or line == '</p>\n':
                continue
            # elif line == '<p>\n' or '<p ' in line:
            #     ids[1] += 1
            #     ids[3] += 1
            #     ids[0] = 0
            #     continue
            # elif line == '<s>\n' or '<s ' in line:
            #     ids[0] += 1
            #     ids[2] += 1
            #     line = '<s id="%d;%d;%d;%d">\n' % tuple (ids)
            else:
                for tag in tags:
                    if '<' + tag + ' n="' in line:
                        start = len (tag) + 5
                        n = line[start : line.find ('"', start)]
                        meta.append ((tag, n))
                        skip = True
                        break
                    else:
                        if '</' + tag in line:
                            if tag == 'case_name':
                                fout.write ('</meta>\n')
                            skip = True
                            break
            if skip:
                skip = False
                continue
            if meta:
                fout.write ('<meta ' + ' '.join ([el[0] + '="' + el[1] + '"' for el in meta]) + '>\n')
                meta = []
                ids[0] = 0
                ids[1] = 0
            if line[-1] != '\n':
                line += '\n'
            fout.write (line)

def getRange (rng):
	if ',' in rng:
		left, right = rng.split (',')
	elif ':' in rng:
		left, right = rng.split (':')
	else:
		left = right = rng
	return int (left), int (right) + 1

def getSents (rng, fl):
	if rng[0] == -1:
		return ['<p>']
	count = 0
	sents = []
	cur_sent = ''
	while count < rng[1] - rng[0]:
		line = fl.readline ().strip ()
		if line == '</p>':
			if cur_sent:
				sents.append (cur_sent.strip ())
				cur_sent = ''
			count += 1
		if line == '</s>':
			sents.append (cur_sent.strip ())
			cur_sent = ''
		elif '<' in line and '>' in line:
			continue
		else:
			cur_sent += line.split ()[0] + ' '
	return sents + ['<p>']

def splitCWB (path):

    dr, fname = os.path.split (path)
    name, ext = os.path.splitext (fname)
    try:
        os.makedirs (os.path.join (dr, name))
    except OSError:
        pass
    with open (path) as fcwb:
        lines = []
        number = ''
        for ind, line in enumerate (fcwb):
            lines.append (line)
            r = re.search (r'<case_number n="([^"]+)', line)
            if r:
                number = r.group (1)
                continue
            if line == '</doc>\n':
                if not number:
                    logging.warning ('case number not found (line: %d)!', ind)
                    lines = []
                else:
                    with open (os.path.join (dr, name, number + ext), 'w') as fpart:
                        fpart.write (''.join (lines))
                        lines = []
                        print (ind)

def cwb2sent (path):

    sents = []
    with open (path) as fcwb:
        sent = ''
        for line in fcwb:
            if line == '</s>\n':
                sents.append (sent.strip ())
                sent = ''
            if not line.startswith ('<'):
                try:
                    sent += line.split ('\t')[0].strip () + ' '
                except:
                    pass

    with open (os.path.splitext (path)[0] + '.sent', 'w') as fout:
        fout.write ('\n'.join (sents) + '\n')

def hunEmAll (path1, path2, make_sents = [False, False], run = False):

    lang1 = os.path.splitext (os.path.split (path1)[1])[0]
    lang2 = os.path.splitext (os.path.split (path2)[1])[0]
    hunPath = r'D:\Praca\narzedzia\alignment\hunalign-1.2\hunalign.exe'
    alignDir = r'D:\Praca\Birma\Dane\align'
    dictPath = r'D:\Praca\narzedzia\alignment\hunalign-1.2\dicts\%s-%s.dic' % (lang1[:2], lang2[:2])
    if not os.path.exists (dictPath):
        dictPath = r'D:\Praca\narzedzia\alignment\hunalign-1.2\dict\null.dic'
    if make_sents[0]:
        print ('cwb2sent:', lang1)
        cwb2sent (path1)
    if make_sents[1]:
        print ('cwb2sent:', lang2)
        cwb2sent (path2)
    batchPath = '%s\\%s-%s.batch' % (os.path.split (path1)[0], lang1, lang2)
    with open (batchPath, 'w') as fbatch:
        fbatch.write ('\n'.join ([os.path.splitext (path1)[0] + str (i + 1) + '.sent' + '\t' +
                                  os.path.splitext (path2)[0] + str (i + 1) + '.sent' + '\t' +
                                  os.path.split (path1)[0] + '\\' + lang1 + '-' + lang2 + str (i + 1) + '.hun' for i in range (chunks)]))
    command = '%s -utf -realign -batch %s %s' % (hunPath, dictPath, batchPath)

    print ('command:')
    print (command[:50])
    print (command[50:])
    if run:
        os.system (command)
    # ladder = []
    # add = [0, 0]
    # for ind, chunk in enumerate (chunks):
    #     with open (os.path.split (path1)[0] + '\\' + lang1 + '-' + lang2 + str (ind + 1) + '.hun') as fhun:
    #         for line in fhun:
    #             try:
    #                 l, r, p = line.strip ().split ('\t')
    #                 l = str (int (l) + add[0])
    #                 r = str (int (r) + add[1])
    #                 ladder.append ((l, r, p))
    #             except:
    #                 continue
    #         add = [int (ladder[-1][0]), int (ladder[-1][1])]
    # if chunks:
    #     with open (os.path.split (path1)[0] + '\\' + lang1 + '-' + lang2 + '.hun', 'w') as fout:
    #         for triple in ladder:
    #             fout.write ('\t'.join (triple) + '\n')

def getCase (fin):

    pat_open = re.compile (r'^<meta .*case_number="([^"])')
    pat_close = re.compile (r'^</meta>')
    number = ''
    case = ''
    for line in fin:
        r = re.search (pat_open, line)
        if r:
            number = r.group (1)
        case += line
        if re.search (pat_close, line):
            return number, case

    return '', ''

def checkAlignmentConsistency (path1, path2, langs):

    tag_open = '<Align_%s_%s>' % (list (sorted (langs)))
    tag_close = '</' + tag_open[1:]

    with open (path1) as fin1, open (path2) as fin2:
        number1, case1 = getCase (fin1)
        number2, case2 = getCase (fin2)
        while number1 and number2:
            while number1 and number1 < case2:
                number1, case1 = getCase (fin1)
            if not case1:
                break
            while number2 and number2 < case1:
                number2, case2 = getCase (fin2)
            if not number2:
                break
            if case1.count (tag_open) != case2.count (tag_open):
                logging.error ('Align mismatch: %s', number1)
                break
            number1, case1 = getCase (fin1)
            number2, case2 = getCase (fin2)

langs = ['BUL', 'CES', 'DAN', 'DEU', 'ELL', 'ENG', 'EST', 'FIN', 'FRA', 'HRV', 'HUN', 'ITA', 'LAV', 'LIT', 'MLT', 'NLD', 'POL', 'POR', 'RON', 'SLK', 'SLV', 'SPA', 'SWE']

if __name__ == '__main__':

    # path_lang1_sent = r'D:\Praca\Birma\tmpteng.ttryxt'
    # path_lang2_sent = r'D:\Praca\Birma\tmptdeu.txt'
    # path_lang1_cwb = r'D:\Praca\Birma\Dane\ENG2.vert'
    # path_lang2_cwb = r'D:\Praca\Birma\Dane\DEU2.vert'
    # path_lang1_out = r'D:\Praca\Birma\neweng.txt'
    # path_lang2_out = r'D:\Praca\Birma\newdeu.txt'
    # path_hun = r'D:\Praca\Birma\eng_deu.hun'
    #
    # splitCWB (path_lang1_cwb)
    #
    # ladder = loadLadder (path_hun)
    # ps = makePs (path_lang1_sent, path_lang2_sent, path_hun)
    # addAlignmentTags (path_lang1_cwb, 'EN', 'DEU', ladder, ps, 0, path_lang1_out)
    # addAlignmentTags (path_lang2_cwb, 'EN', 'DEU', ladder, ps, 1, path_lang2_out)
    # adjustFile (path_lang1_out)
    # adjustFile (path_lang2_out)
    # cwb2sent (path_lang1_cwb, r'', 0, 500000)
    # cwb2sent (path_lang2_cwb, r'', 1, 50000)

    path = r'D:\Praca\Birma\Dane\fromServer'
    langs = ['BUL', 'CES', 'DAN']
    addAlignmentTagsMulti (path, langs)
