# -*- coding: utf-8 -*-

'''Script for encoding CWB aligned corpora.
For each specified language it takes an input file that has specified format
(by default: LANG_alg.vrt, where lang is given language - uppercase). Format
specificatin must contain string {lang} or {l} (lowercase), {LANG} or {L}
(uppercase), which point to position of the language in filename
It performs two steps:

1) cwb-encode
2) cwb-makeall

If the option -a (align) is set, it performs two additional steps for each
unique pair of languages:

3) cwb-align
4) cwb-align-encode

By default the script executes all commands, but it can only write them
to the file which can be run from the shell'''


import os
import glob
import shutil
import os.path as op
import argparse
from itertools import combinations, permutations

def getOptions ():

    ### EDIT DICTIONARY BELOW TO RUN SCRIPT WITHOUT SETTING OPTIONS IN COMMAND LINE ###

    DEFAULT = {
        'align': False,
        'corpus_name': 'BIRM',
        'corpus_path': '/data/Birmingham/all/Corpus',
        'input_format': '{LANG}_alg.cwb.adjusted',
        'input_path': '/data/Birmingham/all/vrt',
        'languages': ['BUL', 'CES', 'DAN', 'DEU', 'ELL', 'ENG', 'EST', 'FIN',
                      'FRA', 'HRV', 'HUN', 'ITA', 'LAV', 'LIT', 'MLT', 'NLD',
                      'POL', 'POR', 'RON', 'SLK', 'SLV', 'SPA', 'SWE'],
        'logging': True,
        'output_path': 'encode.sh',
        'number_of_processes': 1,
        'write_to_file': False,
    }


    options = {}    # options dictionary


    parser = argparse.ArgumentParser () # command line options (higher priority than DEFAULT)
    parser.add_argument ('-i', '--input-path', required = not bool (DEFAULT['input_path']))
    parser.add_argument ('-c', '--corpus-path', required = not bool (DEFAULT['corpus_path']))
    parser.add_argument ('-n', '--corpus-name', required = not bool (DEFAULT['corpus_name']))
    parser.add_argument ('-l', '--languages', nargs = '+', required = not bool (DEFAULT['languages']))
    parser.add_argument ('-L', '--logging', action = 'store_true')
    parser.add_argument ('-w', '--write-to-file', action = 'store_true')
    parser.add_argument ('-o', '--output-path', default = 'encode.sh')
    parser.add_argument ('-f', '--input-format', default = '{LANG}_alg.vrt')
    parser.add_argument ('-a', '--align', action = 'store_true')
    parser.add_argument ('-p', '--number-of-processes', type = int, default = 1)

    args = parser.parse_args ()
    args = vars (args)  # make a dictionary from the argparse object

    for option in DEFAULT:
        if args[option]:
            options[option] = args[option]
        else:
            options[option] = DEFAULT[option]

    options['languages'].sort ()

    return options

if __name__ == '__main__':

    options = getOptions ()

    commands = [
        'cwb-encode -d %s -f %s -R %s -c utf8 -xsB -P tag -P lemma -S s:0+n -S p:0+n -S meta:0+case_name+case_number+date+doc_cellar -S doc -S summary -S parties -S grounds -S costs -S operative_part -S subject -S judgment -S heading %s',
        'cwb-makeall -r %s BIRM_%s',
        'cwb-align -r %s -S Align_%s_%s -o align-%s-%s.out %s %s Align_%s_%s',
        'cwb-align-encode -r %s -D align-%s-%s.out'
    ]

    if options['logging']:  # make logging directory if needed
        if not op.exists (op.join (options['input_path'], 'log')):
            os.makedirs (op.join (options['input_path'], 'log'))

    if not op.exists (op.join (options['corpus_path'], 'Registry')): # make 'Registry' dir
        os.makedirs (op.join (options['corpus_path'], 'Registry'))

    if options['write_to_file']:
        output = [[], [], [], [], []]

    if options['align']:
        cmb = list (combinations (options['languages'], 2))
        align_list = []
        for c in cmb:
            align_list.append ('-S Align_%s_%s' % c)
        align_tags = ' '.join (align_list)
    else:
        align_tags = ''


    for lang in options['languages']:
        data_p = op.join (options['corpus_path'], 'Data', 'birm_%s' % lang.lower ())
        input_p = op.join (options['input_path'], '%s_alg.cwb.adjusted' % lang)
        reg_p = op.join (options['corpus_path'], 'Registry', (options['corpus_name'] + '_' + lang).lower ())

        # remove old data, make necessary directories

        if op.exists (data_p):
            shutil.rmtree (data_p)
        os.makedirs (data_p)
        if op.exists (reg_p):
            os.remove (reg_p)


        # cwb-encode

        command = commands[0] % (data_p, input_p, reg_p, align_tags)
        if options['logging']:
            command += ' > %s_out.log 2> %s_err.out' % (op.join (options['input_path'], 'log', lang), op.join (options['input_path'], 'log', lang))
        print ('Encoding, language: ', lang)
        if options['write_to_file']:
            output[0].append (command)
        else:
            os.system (command)

        #cwb-makeall

        command = commands[1] % (op.join (options['corpus_path'], 'Registry'), lang)
        if options['write_to_file']:
            output[1].append (command)
        else:
            os.system (command)

        # for aligned corpora add ALIGNED field in registry files

        if options['align']:
            if options['write_to_file']:
                for aligned in options['languages']:
                    if aligned != lang:
                        output[2].append ('echo "ALIGNED %s" >> %s' % ((options['corpus_name'] + '_' + aligned).lower (), reg_p))
            else:
                with open (reg_p, 'a') as freg:
                    freg.write ('\n')
                    for aligned in options['languages']:
                        if aligned != lang:
                            freg.write ('ALIGNED %s\n' % (options['corpus_name'] + '_' + aligned).lower ())


    # alignment (optional)

    if options['align']:

        for l1, l2 in permutations (options['languages'], 2):

            # cwb-align

            command = commands[2] % (op.join (options['corpus_path'], 'Registry'),
                                     l1, l2, l1, l2,
                                     (options['corpus_name'] + '_' + l1).upper (),
                                     (options['corpus_name'] + '_' + l2).upper (),
                                     l1, l2)
            print ('Aligning, languages:', l1, l2)
            if options['write_to_file']:
                output[3].append (command)
            else:
                os.system (command)

            # cwb-align-encode

            command = commands[3] % (op.join (options['corpus_path'], 'Registry'), l1, l2)
            if options['write_to_file']:
                output[4].append (command)
            else:
                os.system (command)

if options['write_to_file']:
    with open (options['output_path'], 'w') as fout:
        for section in output:
            for numb, line in enumerate (section):
                if options['number_of_processes'] > 1:
                    if not line.startswith ('echo'):
                        line += ' &'
                    if numb and not numb % options['number_of_processes']:
                        fout.write ('wait\n')
                fout.write (line + '\n')
            if options['number_of_processes'] > 1:
                fout.write ('wait\n')
        fout.write ('rm -f align*.out')

print ('done.')
