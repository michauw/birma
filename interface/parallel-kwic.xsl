<?xml version="1.0" encoding="UTF-8"?>
<!-- THIS IS THE XSLT SHEET TO TRANSFORM QUERY RESULTS FOR DISPLAY IN A CONCORDANCE WINDOW-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="html"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
			<html>
				<head>
				<meta charset="utf-8"/>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<title>Corpus Query results</title>
				<link rel="shortcut icon" href="img/jgu.ico"/>
				<link href="dist/css/bootstrap.min.css" rel="stylesheet"/>
				<link rel="stylesheet" href="dist/css/ngDialog.min.css"/>
				<link href="dist/css/ngDialog-theme-default.min.css" rel="stylesheet"/>
				<link rel="stylesheet/less" type="text/css" href="styl.less"/>
				<link rel="stylesheet" type="text/css" href="css/nav.css"/>
				<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.17.custom.css"/>
				<!--<link rel="stylesheet" href="css/keyboard.css" type="text/css"></link>-->
				<script src="vendor/less.min.js"></script>
				<script src="vendor/ui-bootstrap-tpls-0.12.1.min.js"></script>
				<script src="dist/js/ngDialog.min.js"></script>
				<script type="text/javascript" src="dist/js/lodash.min.js"></script>		
				<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
				<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
				<script type="text/javascript" src="dist/js/bootstrap.min.js"></script>
				<!--<script type="text/javascript" src="js/initDialogs.js"></script>-->
				<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>	
				 <script type="text/javascript">
					 $(document).ready(function() {
						$('td.text').click(function() {
							var myCol = $(this).index();
							console.log ('mc:', myCol);
							var $tr = $(this).closest('tr');
							var myRow = $tr.index();
							var table = $(this).closest('table');
							var lang = $(table).find("tr").eq(1).find("td").eq(myCol).find("b").html();
							var primlang = $(table).find("tr").eq(1).find("td").eq(1).find("b").html();
							var token_nr = $(table).find("tr").eq(myRow).find("td").eq(1).find("font").eq(0).html();
							var corpus = "diego"; //$(table).find("i").eq(0).html();
							var url = 'results_context_xml.php?corpus=' + corpus + '&amp;lang=' + lang + '&amp;primlang=' + primlang + '&amp;langs=fr_es' + '&amp;token_nr=' + token_nr;
							var windowName = corpus + "_" + primlang + ":" + token_nr + " in " + corpus + "_" + lang;
							var windowSize = "width=800,height=800,scrollbars=yes";
							window.open(url, windowName, scrollbars="yes", menubar="no");
						});
					  });				 
					 
					 
					 function post(path, params, method) {
							method = method || "post"; // Set method to post by default if not specified.
							console.log (params.length);
							// The rest of this code assumes you are not using a library.
							// It can be made less wordy if you use one.
							var form = document.createElement("form");
							form.setAttribute("method", method);
							form.setAttribute("action", path);

							for(var key in params) {
								if(params.hasOwnProperty(key)) {
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", key);
									hiddenField.setAttribute("value", params[key]);

									form.appendChild(hiddenField);
								 }
							}
							document.body.appendChild(form);
							form.submit();
						}

					  var allChecked = false;
					  var checkAll = function () {
						$('input.check').attr('checked',!allChecked)
						allChecked = !allChecked;
						if (allChecked === true)
							$('#check_button').attr('value', 'uncheck all');
						else
							$('#check_button').attr('value', 'check all');
					  };
					  var exportCsv = function () {
						var selected = [];
						var langs = [];
						$('tr').first().next().find('td').each (function (){
							langs.push ($(this).text());
						});
						selected.push (langs);
						var trs = $('input.check:checked').closest ('tr');
						$(trs).each (function (){
							tds = $(this).find ('td:not(:first-child)');
							console.log (tds.length);
							tdtexts = [];
							$(tds).each (function (){
								tdtexts.push ($(this).text ().replace(/^\s+|\s+$/g, ''))
							});
							selected.push (tdtexts);
						});
						var jselected = JSON.stringify (selected);
						post ('export_csv.php', {selected: jselected});
					  }
				  
				 </script>
				 	<style>
						.control-button {
							margin: 1px; 
							margin-bottom: 5px; 
							box-shadow: 1px 1px 1px;
						}
					</style>
				</head>
				<body class="indent">
				<!--<xsl:element name="font">
Metadata: <xsl:attribute name="size">+1</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:for-each select="RESULTS/CONCORDANCE/LINE[1]/STRUCS/show">
<xsl:value-of select="name(@*)"/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="', '"/>
</xsl:if>
</xsl:for-each>
</xsl:element>-->
					<div id="naglowek" class="navbar navbar-deafult" role="navigation">
						<div id="Ustja" class="col-lg-12">
							<h1>Diego Corpus</h1>
							<h2></h2>
						</div>
					</div>


					<p align="left" style="font-family:Tahoma; font-size:12px; color:black">
					<b/>
					<xsl:apply-templates/>
					<xsl:variable name="ALLnum-LINE" select="count(//LINE)"/>
					<xsl:value-of select="$ALLnum-LINE"/>
					<xsl:value-of select="' hits overall.'"/> 

					</p>
				</body>
			</html>
		</xsl:template>

		<xsl:template match="RESULTS/text()">
		<xsl:element name="b">
					<xsl:value-of select="'('"/>
			<xsl:value-of select="."/>
					<xsl:value-of select="' hit'"/>
					<xsl:if test=".&gt;1">
					<xsl:value-of select="'s'"/>
					</xsl:if>
					<xsl:value-of select="'.)'"/>
					
		</xsl:element>
		<xsl:element name="p"/>
		</xsl:template>
		
<xsl:template match="CONCORDANCE">
					<div style="margin-bottom: 5px">
						<h1>Results</h1>
					</div>
					<div style="margin-bottom: 5px">
						<input type="button" id="check_button" class="control-button" value="check all" onclick="checkAll()"/>
						<input type="button" id="export_csv" class="control-button" value="export selected (CSV)" onclick="exportCsv()"/>
						<!--<input type="button" id="export_txt" class="control-button" value="export selected (XML)" onclick="export()"/>-->
					</div>
					<table id="ausgabetabelle" style="font-size: small; border-spacing: 1; background-color: #FFEEAA; width: 100%;"> 
					<tr><td></td><td></td><td>
					<xsl:element name="b">					
					<!-- <xsl:variable name="num-LINE" select="count(.//LINE)"/> 
					<xsl:value-of select="$num-LINE"/>
					<xsl:value-of select="' hit'"/>
					<xsl:if test="$num-LINE>1">
					<xsl:value-of select="'s'"/>
					</xsl:if>
					-->
					<!--
					<xsl:value-of select="'Metadata format: '"/>
					<xsl:call-template name="meta-format"/>
					</xls:call-template>
					-->
					</xsl:element>									
					</td></tr>
					<!-- Überschrift -->
					<xsl:element name="tr">
					<xsl:element name="td"/>
					<xsl:element name="td">
					<b>
					<xsl:value-of select="/RESULTS/@primlang"/>
					</b>
					</xsl:element>
					<xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
					<xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
					<xsl:element name="td">
					<b>
					<xsl:value-of select="substring-after(@name, '_')"/>
					</b>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					
					<xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
					<xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
					<xsl:for-each select="LINE">
					<xsl:element name="tr">
					<xsl:if test="(position() mod 2)=1">
					<xsl:attribute name="style">color:blue; background-color: #FFFFFF;</xsl:attribute>
					</xsl:if>
					<xsl:if test="(position() mod 2)=0">
					<xsl:attribute name="style">color:blue; background-color: #FFDDAA;</xsl:attribute>
					</xsl:if>
					
					<xsl:variable name="aligInfo" select="CONTENT/MATCH/COLLOCATE"/>
						<xsl:element name="td">
							<xsl:attribute name="style">background-color:white;</xsl:attribute>
							<xsl:element name="input">
								<xsl:attribute name="type">checkbox</xsl:attribute>
								<xsl:attribute name="class">check</xsl:attribute>
								<xsl:attribute name="style">margin-right:5px;</xsl:attribute>
							</xsl:element>
						</xsl:element>

					<xsl:element name="td">
						<xsl:attribute name="class">text</xsl:attribute>
						<xsl:attribute name="style">padding: 3px;</xsl:attribute>
					<xsl:apply-templates select="MATCHNUM"/>
					<xsl:apply-templates select="CONTENT"/>
					<xsl:apply-templates select="STRUCS"/>
					</xsl:element>
					<xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
					<xsl:element name="td">
						<xsl:attribute name="class">text</xsl:attribute>
					<xsl:attribute name="width">
					<xsl:value-of select="(100 div ($numberColumns+1))"/>
					<xsl:value-of select="'%'"/>
					</xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
					<xsl:attribute name="title"></xsl:attribute>
					<xsl:call-template name="doToken">
					<xsl:with-param name="aligInfo" select="$aligInfo"/>
					</xsl:call-template>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					</xsl:for-each>
					</table>					
</xsl:template>

<xsl:template name="doToken">
<xsl:param name="aligInfo"/>
<xsl:for-each select=".//TOKEN">
<xsl:if test="not ((starts-with(text(),  '.')) or (starts-with( text(), '?')) or (starts-with(text(), '!')) or (starts-with(text(), ',')) or (starts-with(text(), ':')) or (starts-with(text(), ';')) or (starts-with(text(), ':')))">
<xsl:value-of select="' '"/>
</xsl:if>
<xsl:variable name="id" select="substring-before(substring-after(substring-after(ANNOT,'/'),'/'), '/')"/>
<xsl:variable name="PASS" select="concat('=w:', text(), '=i:', $id)"/>


<!-- <xsl:message><xsl:value-of select="$PASS"/></xsl:message>-->
<!-- <xsl:message><xsl:value-of select="$PASS"/></xsl:message> -->
<xsl:element name="font">
<xsl:attribute name="title">
<xsl:value-of select="ANNOT"/>
</xsl:attribute>

<xsl:if test="contains($aligInfo/TOKEN/ANNOT, $PASS)">
<xsl:attribute name="style">font-weight: bold</xsl:attribute>
</xsl:if>
<xsl:value-of select="text()"/>
</xsl:element>
</xsl:for-each>
</xsl:template>

<!--
<xsl:template name="metaFormat">
<xsl:param name="strucsNode"/>
<xsl:for-each select="show">
<xsl:value-of
</xsl:for-each>
</xsl:template>
-->


<xsl:template match="MATCHNUM">
<font color="red" style="display:none">
<xsl:apply-templates/>
</font>
</xsl:template>

<xsl:template match="MATCH">
<font color="red" >
<b>
<xsl:apply-templates/>
</b>
</font>
</xsl:template>

<xsl:template match="STRUCS">
<!--
<xsl:element name="font">
<xsl:attribute name="size">-4</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:attribute name="title">
<xsl:for-each select="show[not(@chapter_author or @chapter_title or @chapter_pub_year)]">
<xsl:value-of select="."/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="' : '"/>
</xsl:if>
</xsl:for-each>
</xsl:attribute>
[
<xsl:if test="show[@chapter_author]">
<xsl:value-of select="show[@chapter_author]"/>
<xsl:value-of select="': '"/>
</xsl:if>
<xsl:if test="show[@chapter_title]">
<xsl:value-of select="show[@chapter_title]"/>
</xsl:if>
<xsl:if test="show[@chapter_pub_year]">
<xsl:value-of select="show[@chapter_pub_year]"/>
</xsl:if>
]
</xsl:element>
-->
<xsl:element name="font">
<xsl:attribute name="size">-4</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:attribute name="title">
<xsl:for-each select="show">
<xsl:value-of select="name(@*[1])"/>
<xsl:value-of select="': '"/>
<xsl:value-of select="."/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="'&#10;'"/>
</xsl:if>
</xsl:for-each>
</xsl:attribute>

<xsl:value-of select="' ['"/>
<xsl:for-each select="show">
<xsl:value-of select="."/>	
<xsl:if test="position() &lt; last() and string-length(.) != 0">
<xsl:value-of select="', '"/>
</xsl:if>
</xsl:for-each>
<xsl:value-of select="']'"/>
</xsl:element>



</xsl:template>


<xsl:template match="TOKEN">
<xsl:if test="not ((starts-with(text(),  '.')) or (starts-with( text(), '?')) or (starts-with(text(), '!')) or (starts-with(text(), ',')) or (starts-with(text(), ':')) or (starts-with(text(), ';')) or (starts-with(text(), ':')))">
<xsl:value-of select="' '"/>
</xsl:if>
<xsl:element name="font">
<xsl:attribute name="title">
<xsl:value-of select="ANNOT"/>
</xsl:attribute>
<xsl:value-of select="text()"/>
</xsl:element>
</xsl:template>

<xsl:template match="s">
<xsl:element name="sup">
<xsl:value-of select="@id"/>
</xsl:element>
</xsl:template>
	</xsl:stylesheet>
