corpus.directive("autoComplete", ['autoCompleteDataService', function(autoCompleteDataService) {
	return {
		restrict: 'A',
		scope: {
			queryRow: '=',
			at_type: '=',
			language: '='
		}
		link: function(scope, elem, attr, ctrl) {
			elem.autocomplete({
				alert('a');
				source: autoCompleteDataService.getData($scope.queryRow.token, $scope.at_type, $scope.language),
				minLength: 2
			});
		}
	};
}]);