corpus.directive('languageQuery', ['stringProcessor', 'queryKeeper', 'metaKeeper', 'sectionKeeper', 'modeKeeper', 'configData', 'gettextCatalog', 'ngDialog', function(stringProcessor, queryKeeper, metaKeeper, sectionKeeper, modeKeeper, configData, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/languageQuery.html',
        restrict: 'E',
        scope: {
            language: '=',
            corp: '=',
        },
        controller: function($scope, gettextCatalog, configData) {

            var lastQuery = '';
            var sourceFieldName = 'chapter_src';
            queryKeeper.setCqpChanged(false);

            $scope.metaVisible = false;
            $scope.metaButton = 'Show';
            $scope.mode = modeKeeper.get ('name');
			$scope.outputQuery = '';

            $scope.getCorpus = function () {
                return modeKeeper.getCorpus ();
            }

            $scope.fullNameOf = function(language){
                var translation = gettextCatalog.getString(configData.getLangs(language.name));
                if (gettextCatalog.getCurrentLanguage() == 'pl_PL' && translation.indexOf('i', translation.length - 1) !== -1)
                    translation += 'ego';
                return translation;
            }

            $scope.exactPhraseQuery = function() {
                var quellText = stringProcessor.removeSpaces($scope.exactQuery);
                $scope.exactQuery = quellText;
                quellText = stringProcessor.replaceAndEscapeChrs(quellText);
                var resultText = "\"";
                var i = 0;
                // make from 'hallo you' query : '"hallo" "you"'
                while (i < quellText.length) {
                    var ch = quellText.charAt(i);
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    resultText += ch;
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    i++;
                }
                resultText += "\"";
                if ($scope.exactQueryCaseSensitive == true)
                    resultText = resultText + "%c";
                $scope.exactQueryOutput = resultText;
            }

            $scope.$watchCollection('{eq: exactQuery, cs: exactQueryCaseSensitive}', $scope.exactPhraseQuery);

            $scope.getVisible = function() {
                return $scope.metaVisible;
            }

            $scope.changeVisible = function() {
                $scope.metaVisible = !$scope.metaVisible;
                if ($scope.metaVisible == true)
                    $scope.metaButton = 'Hide';
                else
                    $scope.metaButton = 'Show';
            }

            $scope.clear = function() {
                queryKeeper.clear($scope.language);
				$scope.$broadcast ('qR change');
				$scope.outputQuery = '';
            }

            $scope.moreRows = function() {

                queryKeeper.push($scope.language);

            };

            $scope.lessRows = function() {
                queryKeeper.pop($scope.language);
            };

            $scope.rowsNumber = function() {
                var myArr = [];
                for (i=0; i<queryKeeper.getLength($scope.language); myArr.push(i++));
                    return myArr;
            }

            $scope.allRows = function(){
                return queryKeeper.getAll($scope.language);
            }

            $scope.getQuery = function() {
                var query = queryKeeper.getQuery ($scope.language);
                if (query === '') // if Cqp Search Field was manually edited, don't update it without user confirmation
                    return $scope.outputQuery;
                return query;
            }

            $scope.checkManualUpdate = function() {
                queryKeeper.setCqpChanged(true);
            }

            $scope.getPrimary = function(){
                return queryKeeper.getPrimaryLanguage();
            }

            $scope.setPrimary = function(newValue){
                return queryKeeper.setPrimaryLanguage(newValue);
            }

            // $scope.setAsSourceLanguage = function(newValue) {
			// 	return queryKeeper.setAsSourceLanguage(sourceFieldName, newValue);
            // }

            $scope.setSection = function (newValue) {
                return sectionKeeper.setSection (newValue);
            }

            $scope.isSet = function (section) {
                return sectionKeeper.getSection () === section;
            }

            $scope.getButtonState = function() {
                return gettextCatalog.getString($scope.metaButton);
            }

            $scope.getMetaVisibility = function(){
                var resp = "";
				if (queryKeeper.getPrimaryLanguage() != $scope.language)
                    resp = "hidden";
                return resp;
            }

          $scope.metaToShow = function() {
              var meta = metaKeeper.getAll();
              var results = [];
              for (var i = 0; i < meta.length; ++i) {
                  if (meta[i].inResults == true)
                  results.push(meta[i].name);
              }
              return JSON.stringify(results);
          }

          $scope.queryNegation = queryKeeper.getNegation ($scope.language.name);

          $scope.$watch('queryNegation', function(newValue) {
              queryKeeper.setNegation($scope.language.name, newValue);
          });

          $scope.$watch('getQuery()',function(nval){
              $scope.outputQuery = nval;
          });

          $scope.updateQuery = function() {
          }

          $scope.exactQueryCaseSensitive = true;

          $scope.metaFieldsNumber = function() {
              return metaKeeper.getLength();
          }

          $scope.metaFieldsNumberArray = function() {
              var tmpArray = [];
              for (i = 0; i < metaKeeper.getLength() / 3; tmpArray.push(i++));
              return tmpArray;
          }

          //initialize query rows
          if (typeof (queryKeeper.getAll ($scope.language)) === 'undefined')
            $scope.moreRows();


        }
    };
}]);
