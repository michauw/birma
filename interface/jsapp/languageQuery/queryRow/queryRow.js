corpus.directive('queryRow', ['queryKeeper', 'ngDialog', function(queryKeeper, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/queryRow/queryRow.html',
        restrict: 'E',
        scope: {
            showTokensInBetween: '=',
            index: '=',
            language: '=',
        },
        controller: function($scope, $rootScope) {

			var popUpOpened = false;
			var showWarning = true;
            $scope.queryRow = queryKeeper.get($scope.language, $scope.index);
            $scope.$watchCollection('queryRow', function(newValue, oldValue) {
				if (!showWarning)
					queryKeeper.setCqpChanged (false);
				if (queryKeeper.getCqpChanged() && !popUpOpened && showWarning)
				{
					popUpOpened = true;
					ngDialog.openConfirm({
						template: 'jsapp/languageQuery/queryRow/modal.html',
						className: 'ngdialog-theme-default'
					}).then(function (hide) {
						queryKeeper.setCqpChanged (false);
						queryKeeper.set($scope.language, $scope.index, newValue);
						popUpOpened = false;
						showWarning = !hide;
					}, function (hide) {
						queryKeeper.set($scope.language, $scope.index, oldValue);
						popUpOpened = false;
						showWarning = !hide;
					});
				}
				else
				{
					queryKeeper.set($scope.language, $scope.index, newValue);
				}
            });
			$scope.getFields = function (){
				return queryKeeper.get($scope.language, $scope.index);
			}
			$scope.autocmp = function(ids, atype) {
				$scope.atype = atype;
				getSuggestion (ids, atype, $scope.language, $scope);
				// console.log ('auto:', ids, atype);
			}
			$scope.$on ('qR change', function () {
				$scope.queryRow = queryKeeper.get ($scope.language, $scope.index);
			});
        }
    };
}]);
