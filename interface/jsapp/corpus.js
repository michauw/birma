var corpus = angular.module ('corpus', ['ngAnimate', 'ui.bootstrap', 'gettext', 'ngDialog', 'ngRoute', 'ngCookies', 'ui.bootstrap.pagination']);

corpus.config (function ($routeProvider) {
    $routeProvider
    .when ('/', {
        templateUrl: 'jsapp/queryPage/queryPage.html',
        controller: 'main',
        resolve: {
            languages: function ($http) {
                return $http.get ('settings/languages.json').then (function (response) {
                    return response.data;
                });
            },
            metaFields: function ($http) {
                return $http.get ('settings/meta.json').then (function (response) {
                        return response.data;
                });
            },
            metaFieldsAU: function ($http) {
                return $http.get ('settings/metaAU.json').then (function (response) {
                        return response.data;
                });
            },
            metaFieldsFR: function ($http) {
                return $http.get ('settings/metaFR.json').then (function (response) {
                        return response.data;
                });
            },
            metaFieldsUK: function ($http) {
                return $http.get ('settings/metaUK.json').then (function (response) {
                        return response.data;
                });
            }
        }
    })
    .when ('/results', {
        templateUrl: 'jsapp/results/results.html',
        controller: 'resultsController'
    })
    .when ('/frequency', {
        templateUrl: 'jsapp/results/frequency.html',
        controller: 'frequencyController'
    })
    .when ('/collocations', {
        templateUrl: 'jsapp/results/collocations.html',
        controller: 'collocationsController'
    })
    .when ('/ngrams', {
        templateUrl: 'jsapp/results/ngrams.html',
        controller: 'ngramsController'
    })
    .otherwise ({redirectTo:'/'});
});


corpus.run (function ($rootScope, $window, $route, $location, gettextCatalog, queryKeeper, queryHist) {

    gettextCatalog.setCurrentLanguage('en_EN');
    $rootScope.stackPosition = 0;
    $rootScope.$on('$locationChangeSuccess', function() {
    $rootScope.actualLocation = $location.path();

    });
    $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {

        //true only for onPopState
        if($rootScope.actualLocation === newLocation) {
            var back,
                historyState = $window.history.state;
            back = !!(historyState && historyState.position <= $rootScope.stackPosition);

            if (back) {
                //back button
                $rootScope.stackPosition--;
                var change = queryHist.get ($rootScope.stackPosition, -1);
                if (change != 0)
                    queryKeeper.setAll (change);

            } else {
                //forward button
                $rootScope.stackPosition++;
                var change = queryHist.get ($rootScope.stackPosition, 1);
                if (change != 0)
                    queryKeeper.setAll (change);

            }
        } else {
            //normal-way change of page (via link click)
            if ($route.current) {
                $window.history.replaceState({
                    position: $rootScope.stackPosition
                }, '');
                $rootScope.stackPosition++;
            }
        }
     });
});

corpus.controller('main', ['$scope','queryKeeper', 'metaKeeper', 'settingsKeeper', 'modeKeeper', 'gettextCatalog', 'ngDialog', '$http', 'languages', 'metaFields', 'metaFieldsAU', 'metaFieldsFR', 'metaFieldsUK', function($scope, queryKeeper, metaKeeper, settingsKeeper, modeKeeper, gettextCatalog, ngDialog, $http, languages, metaFields, metaFieldsAU, metaFieldsFR, metaFieldsUK) {

    //$scope.available_corpora = [];

    $scope.subcorpora = [];
    $scope.mode = modeKeeper.get('name');

    //$scope.frequency = {'countBy': 'word'};
    //$scope.collocations = {'ams': {'llr': true, 'mi': true, 't-score': true, 'z-score': true, 'dice': true, 'mi3': true, 'frequency': true}, 'countBy': 'word', 'leftContextSize': 3, 'rightContextSize': 3, 'threshold': 5};
    //$scope.ngrams = {'size': 3, 'countBy': 'word', 'leftContextSize': 3, 'rightContextSize': 3, 'threshold': 5};
    //$scope.collocations = {'countBy': 'word', 'leftContextSize': 3, 'rightContextSize': 3}
    //$scope.results_settings =

    queryKeeper.setLanguages (languages);
    $scope.alignedLanguages = queryKeeper.getAlignedLanguages ();

    $scope.settings = settingsKeeper.get ();
    settingsKeeper.set ($scope.settings);

    $scope.$watchCollection ('settings', function (newValue, oldValue) {
        if (newValue != oldValue)
            settingsKeeper.set ($scope.settings);
    }, true)
    $scope.getSettings = function () {

        return settingsKeeper.get ();
    }
    $scope.toggleSettings = function () {
        $scope.settings.open = !$scope.settings.open;
    }
    $scope.toggleMode = function () {

        var newMode;
        if ($scope.mode == 'parallel')
            newMode = 'mono';
        else
            newMode = 'parallel';
        modeKeeper.set ('name', newMode);
        $scope.mode = newMode;

        queryKeeper.setLanguages (languages, newMode);

        queryKeeper.clearAll (true);

        metaKeeper.setFields ($scope.metaMulti[modeKeeper.getCorpus ()]);
    }

    $scope.metaFields = metaFields;
    $scope.metaMulti = {'parallel': metaFields, 'au': metaFieldsAU, 'fr': metaFieldsFR, 'uk': metaFieldsUK};
    $scope.corpora = ['parallel', 'au', 'fr', 'uk'];
    $scope.currentCorpusIndex = 0;
    metaKeeper.setFields ($scope.metaMulti[$scope.corpora[$scope.currentCorpusIndex]]);

    for (var i = 0; i < $scope.alignedLanguages.length; ++i) {
        var lang = $scope.alignedLanguages[i];
        if (lang.primary === true)
            queryKeeper.setPrimaryLanguage (lang);
        var span = document.getElementById ('lang_' + lang.name);
        //span.className = 'pull-right glyphicon glyphicon-ok-circle';
    }

    $scope.getAlignedLanguages = function() {
        return queryKeeper.getAlignedLanguages ();
    }

    $scope.getLanguages = function() {
        return queryKeeper.getLanguages ($scope.mode);
    }

    var twoByTwo = function (list) {
        var out = [];
        for (var i = 0; i < list.length; i += 2)
            out.push (list.slice (i, i + 2));

        return out;
    }

    $scope.tuples = twoByTwo ($scope.alignedLanguages);

    $scope.$watch ('getAlignedLanguages()', function (newValue) {

        $scope.tuples = twoByTwo (newValue)
    });

    $scope.getPrimary = function(){
        return queryKeeper.getPrimaryName ();
    }

    $scope.switchLanguage = function (lang) {
        gettextCatalog.setCurrentLanguage (lang.name);
    }

    $scope.setLang = function (lang) {
        if (lang.name == $scope.primaryLanguage)
            showAlert ("<p>You can't remove primary language!</p>", true)
        else
        {
            var old = alignedLanguages[lang.name].use;
            alignedLanguages[lang.name].use = !old;
            var span = document.getElementById ('lang_' + lang.name);
            if (old === false)
                span.className = 'pull-right glyphicon glyphicon-ok-circle';
            else
                span.className = 'pull-right glyphicon glyphicon-remove-circle';
        }
    }

    $scope.setSubcorpus = function (name) {
        var now;
        for (var i = 0; i < $scope.subcorpora.length; ++i)
            if ($scope.subcorpora[i].name == name)
            {
                now = !$scope.subcorpora[i].use;
                $scope.subcorpora[i].use = now;
                break;
            }
        queryKeeper.setSub ($scope.subcorpora);
        var span = document.getElementById ('subcorpus_' + name);
    }

    $scope.multiSettings = {buttonClasses: 'btn btn-default subcorpora', dynamicTitle: false, showUncheckAll: false, idProp: "id"};

    showAlert = function (templ, plain) {
        ngDialog.open({
        template: templ,
        overlay: false,
        plain: plain
        })
    }
}]);

corpus.factory('queryKeeper', ['metaKeeper', 'sectionKeeper', function (metaKeeper, sectionKeeper) {

    var queryRows = [];
    var primaryLanguage;
    var cqpChanged = false;
    var alignedLanguages = [];
    var parallelLanguages = [];
    var monoLanguages = [];
    var primaryLanguage;
    var languages;
    var backup_query = [];

    var queryNegation = [];
    var basicStateQR = {
        from: '',
        to: '',
        token: '',
        lexeme: '',
        gramTag: '',
        currentRowQueryStringWith: '',
        endWith: '',
        caseSensitive: false,
    };

    return {

			add: function (lang, words) {
				queryRows[lang.name] = queryRows[lang.name].concat (words);
			},
            clear: function (lang, all) {

                queryRows[lang.name] = [Object.assign ({}, basicStateQR)];

                if (all === true) {
                    metaKeeper.clear ();
                    sectionKeeper.clear ();
                }
				console.log ('qR:', queryRows);
            },
            clearAll: function (all) {

                for (var i = 0; i < languages.length; ++i)
                {
                    queryRows[languages[i].name] = [Object.assign ({}, basicStateQR)];
                }
                if (all === true) {
                    metaKeeper.clear ();
                    sectionKeeper.clear ();
                }
            },
            getQuery: function (lang) {
                if (backup_query.length != 0) {       // !! DIRTY HACK (necessary when going back in browser)
                    queryRows = backup_query;
                    backup_query = [];
                }
                if (cqpChanged)
                    return '';
                var query = this.prepareQuery (lang);
                if (query != '' && lang == primaryLanguage) {
                    query += metaKeeper.add () + sectionKeeper.add ();
                }
                return query;
            },
            get: function(lang, i) {
                return queryRows[lang.name][i];
            },
            getAlignedLanguages: function () {
                return alignedLanguages;
            },
            getLanguages: function (mode = 'parallel') {
                if (mode == 'parallel')
                    return parallelLanguages;
                return monoLanguages;
            },
            getAll: function(lang) {
                return queryRows[lang.name];
            },
            getNegation: function(lang) {
                return queryNegation[lang.name];
            },
            getCqpChanged: function () {
                return cqpChanged;
            },
            getPrimaryLanguage: function(){
                return primaryLanguage;
            },
            getPrimaryName: function () {
                return primaryLanguage.name;
            },
            getQueryRows: function () {
                return queryRows;
            },
            getSub: function (query) {



                if (subcorpora.length == subNumber)
                    return '';
                else {
                        var subText = 'match.sub_id="';

                        for (var i = 0; i < subcorpora.length; ++i)
                        {
                            if (subcorpora[i].use === true)
                                subText += subcorpora[i]['name'] + '|';
                        }
                        if (subText[subText.length - 1] === '|')
                            subText = subText.slice(0, -1);
                        subText += '"';
                        var match = query.search ('::');

                        if (match != -1)
                            return ' & ' + subText;
                        else
                            return '::' + subText;
                }
            },
            set: function(lang, index, item) {
                if (!(queryRows[lang.name] instanceof Array)) {
                    queryRows[lang.name] = [];
                }

                queryRows[lang.name][index] = item;

                return item;
            },
            setAll: function (rows) {
                queryRows = rows;
            },
            setBackup: function () {
                backup_query =  Object.assign ([], queryRows);
            },
            setLanguages: function (langs, mode = 'parallel', partial = false) {

                if (partial === true) {
                    for (var i = 0; i < langs.length; ++i)
                    {
                        for (var j = 0; j < languages.length; ++j)
                            if (langs[i].name == languages[j].name && languages[j].type == mode)
                                languages[j] = langs[i];
                    }
                }
                else
                    languages = langs;

                alignedLanguages = [];
                parallelLanguages = [];
                monoLanguages = [];
                var primary_found = false;
                for (var i = 0; i < languages.length; ++i) {
                    if (languages[i].type === 'parallel')
                        parallelLanguages.push (languages[i]);
                    else
                        monoLanguages.push (languages[i]);
                    if (languages[i].use && languages[i].type === mode) {
                        if (languages[i] == primaryLanguage) {
                            alignedLanguages.splice (0, 0, languages[i]);
                            primary_found = true;
                        }
                        else
                            alignedLanguages.push (languages[i]);
                    }
                }
                if (!primary_found && alignedLanguages.length > 0){
                    this.setPrimaryLanguage (alignedLanguages[0]);
                }

            },
            setNegation: function(lang, value) {
                queryNegation[lang.name] = value;
            },
            setCqpChanged: function (value) {
                cqpChanged = value;
            },
            setPrimaryLanguage (lang) {
                primaryLanguage = lang;
                var new_order = [];
                new_order.push (lang);
                for (var i = 0; i < alignedLanguages.length; ++i) {
                    var l = alignedLanguages[i];
                    if (new_order.indexOf (l) < 0)
                        new_order.push(l);
                }
                alignedLanguages = new_order;
            },
            setSub: function (sub) {
                subcorpora = sub;
                var sn = 0;
                for (var i = 0; i < subcorpora.length; ++i)
                    if (subcorpora[i].use === true)
                        sn += 1;
                subNumber = sn;
            },
            setSubNumber: function (value) {

                subNumber = value;
            },
            pop: function(lang) {
                if (queryRows[lang.name].length > 1) queryRows[lang.name].pop();
            },
            push: function(lang) {

                if (!(queryRows[lang.name] instanceof Array)) {
                    queryRows[lang.name] = [];
                }

                queryRows[lang.name].push(Object.assign ({}, basicStateQR));
            },
            getLength: function (lang) {
                if (!(queryRows[lang.name] instanceof Array)) {
                    queryRows[lang.name] = [];
                }
                return queryRows[lang.name].length;
            },
            prepareQuery: function (lang) {
                var outputQuery = '';
				var alternative = null;
                for (var i in queryRows[lang.name]) {
					var al = queryRows[lang.name][i].alternative;
					if (al > 0)
					{
						alternative = al;
						outputQuery += '(';
						continue;
					}
                    if (typeof(queryRows[lang.name][i]) == 'undefined')
						continue;
                    var currentRowQueryString = '';
                    queryRows[lang.name][i].token = queryRows[lang.name][i].token.replace(/ /g, "");
                    queryRows[lang.name][i].lexeme = queryRows[lang.name][i].lexeme.replace(/ /g, "");
                    queryRows[lang.name][i].gramTag = queryRows[lang.name][i].gramTag.replace(/ /g, "");


                    if (queryRows[lang.name][i].token != '') {
                        if (queryRows[lang.name][i].endWith) currentRowQueryString += '.*';
                        currentRowQueryString += queryRows[lang.name][i].token;
                        if (queryRows[lang.name][i].beginWith) currentRowQueryString += '.*';

                        currentRowQueryString = 'word="' + currentRowQueryString + '"';

                        if (queryRows[lang.name][i].caseSensitive == false)
                            currentRowQueryString += "%c";
                    }

                    if (queryRows[lang.name][i].lexeme != '') {
                        if (currentRowQueryString != "") currentRowQueryString += " & ";
                        currentRowQueryString += ' lemma="' + queryRows[lang.name][i].lexeme + '"';
                    }

                    if (queryRows[lang.name][i].gramTag) {
                        if (currentRowQueryString != "") currentRowQueryString += " & ";
                        currentRowQueryString += ' tag="' + queryRows[lang.name][i].gramTag + '\"';
                    }

                    if (currentRowQueryString != '') {
                        outputQuery += "[" + currentRowQueryString + "]";
                        if (queryRows[lang.name][i].from < queryRows[lang.name][i].to && queryRows[lang.name][i].to > 0) {
                            outputQuery += '[]{' + (queryRows[lang.name][i].from) + ',' + (queryRows[lang.name][i].to) + '}';
                        }
                    }
                    if ((queryRows[lang.name][i].token != '' || queryRows[lang.name][i].lexeme != '' || queryRows[lang.name][i].gramTag != '') && queryNegation[lang.name]) outputQuery = '!' + outputQuery;
					if (alternative !== null)
					{
						alternative--;
						if (alternative == 0)
							outputQuery += ') | (';
					}

                }
				if (alternative !== null)
					outputQuery += ')';

                return outputQuery;

            }
    }
}]);

corpus.factory ('metaKeeper', function () {

    var metaFields = [];

    return {
            add: function() {

                var metaList = [];
                for (var i = 0; i < metaFields.length; ++i)
                {
                    if (metaFields[i].value == '') continue;
                    metaList.push ('match.' + metaFields[i].name + '="' + metaFields[i].value + '"');
                }
                if (metaList.length != 0) return '::' + metaList.join(' & ');

                return '';
            },
            clear: function () {

                metaFields = [];
            },
            get: function (i, place) {
                return metaFields[i * 3 + place];
            },
            getAll: function() {
        return metaFields;
        },
        getLength: function() {
            return metaFields.length;
        },
        set: function(i, place, item) {

            metaFields[i * 3 + place] = item;
            return item;
        },
        setFields: function (fields) {

            metaFields = fields;
        }
    }
});

corpus.factory ('sectionKeeper', function () {

    var sectionFilter = '';

    return {
        add: function() {
            var filter = '';
            if (sectionFilter != '')
            filter = ' within ' + sectionFilter;
            return filter;
        },
        clear: function () {
            sectionFilter = '';
        },
        getSection: function () {
            return sectionFilter;
        },
        setSection: function(newValue) {
            if (newValue != sectionFilter)
                sectionFilter = newValue;
            else
                sectionFilter = '';
        }
    }
});

corpus.factory('stringProcessor', function() {
    return {
        removeSpaces: function(str) {
            if (typeof(str) == 'undefined') return '';
            var bufStr;
            var currentRowQueryString = 0;
            var end = str.length - 1;
            while (str[currentRowQueryString] == " ") currentRowQueryString++;
            while (str[end] == " ") end--;
            return str.substring(currentRowQueryString, end + 1);
        },
        replaceAndEscapeChrs: function(value) {
            if (typeof(value) == 'undefined') return '';
            value = value.replace(/\(/g, "\\(");
            value = value.replace(/\)/g, "\\)");
            value = value.replace(/\[/g, "\\[");
            value = value.replace(/\]/g, "\\]");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\+/g, "\\+");
            value = value.replace(/\_/g, "\\_");
            value = value.replace(/\-/g, "\\-");
            value = value.replace(/\*/g, "\\*");
            value = value.replace(/\./g, "\\.");
            value = value.replace(/\:/g, "\\:");
            value = value.replace(/\;/g, "\\;");
            value = value.replace(/\,/g, "\\,");
            value = value.replace(/\?/g, "\\?");
            value = value.replace(/\!/g, "\\!");
            return value;
        }
    };
});

corpus.factory('autoCompleteDataService', ['$http', function() {

    var responseData = [];
    var lastQuery = '';

    return {
        getData: function(query, attribute, language) {

    query = query.replace(/ /g, "");
    if (query.length == 0)
    return responseData;
    query = '[' + attribute + '="' + query[0] + '.*"] ';
    if (query == lastQuery)
    return responseData
    lastQuery = query;
    $http.get('autocomplete.php', {
    params: {query : query, attribute: at_type, language : language}
    })
    .success(function(data) {
    responseData = data;
    });

    return responseData;
    }
    };
}]);

corpus.factory('loadLanguages', ['$http', '$q', function(http, q) {

    return {
    getJsonFile: function(path) {
    var ldata;
    var deferred = q.defer();

    http.get(path).then (function(data) {
    deferred.resolve(data);
    });
    return deferred.promise;
    }
    };
}]);

corpus.factory('loadMetaData', ['$http', '$q', function(http, q) {

    return {
    getJsonFile: function(path) {
    var mdata;
    var deferred = q.defer();

    http.get(path).then (function(data) {
    deferred.resolve(data);
    });
    return deferred.promise;
    }
    };
}]);

corpus.factory('configData', ['gettext', function(gettext) {

    var langs = {bg: gettext("Bulgarian"),
     bul: gettext("Bulgarian"),
     be: gettext("Belorussian"),
     bel: gettext("Belorussian"),
     cs: gettext("Czech"),
     cz: gettext("Czech"),
     ces: gettext("Czech"),
     hr: gettext("Croatian"),
     hrv: gettext("Croatian"),
     mk: gettext("Macedonian"),
     mkd: gettext("Macedonian"),
     pl: gettext("Polish"),
     pol: gettext("Polish"),
     ru: gettext("Russian"),
     rus: gettext("Russian"),
     sk: gettext("Slovak"),
     slk: gettext("Slovak"),
     sl: gettext("Slovene"),
     slv: gettext("Slovene"),
     sr: gettext("Serbian"),
     srp: gettext("Serbian"),
     uk: gettext("Ukrainian"),
     ukr: gettext("Ukrainian"),
     us: gettext("Upper Sorbian"),
     da: gettext("Danish"),
     dan: gettext("Danish"),
     de: gettext("German"),
     deu: gettext("German"),
     en: gettext("English"),
     eng: gettext("English"),
     nl: gettext("Dutch"),
     nld: gettext("Dutch"),
     no: gettext("Norwegian"),
     nor: gettext("Norwegian"),
     sv: gettext("Swedish"),
     swe: gettext("Swedish"),
     es: gettext("Spanish"),
     spa: gettext("Spanish"),
     fr: gettext("French"),
     fra: gettext("French"),
     it: gettext("Italian"),
     ita: gettext("Italian"),
     pt: gettext("Portuguese"),
     por: gettext("Portuguese"),
     ro: gettext("Romanian"),
     ron: gettext("Romanian"),
     lt: gettext("Lithuanian"),
     lit: gettext("Lithuanian"),
     lv: gettext("Latvian"),
     lav: gettext("Latvian"),
     et: gettext("Estonian"),
     ese: gettext("Estonian"),
     el: gettext("Greek"),
     ell: gettext("Greek"),
     eo: gettext("Esperanto"),
     epo: gettext("Esperanto"),
     fi: gettext("Finnish"),
     fin: gettext("Finnish"),
     hu: gettext("Hungarian"),
     hun: gettext("Hungarian"),
     hy: gettext("Armenian"),
     hye: gettext("Armenian")
     };

    return {
    getLangs: function (lang) {
    return langs[lang];
    }
    };
}]);

// corpus.factory ('shareDistData', function () {
//     var data;
//
//     return {
//         put: function (d) {
//             data = d;

//         },
//         get: function () {

//             return data;
//         }
//     }
// });

corpus.controller ('resultsController', ['$scope', 'processResults', 'settingsKeeper', 'modeKeeper', function ($scope, processResults, settingsKeeper, modeKeeper) {

    var settings = settingsKeeper.get ();
	console.log ('RESULTS!!');

    $scope.results = [];
    $scope.pagination = {
                            currentPage: 1,
                            numPerPage: settings.results.perPage,
                            maxSize: 5,
                            boundaries: false
                        };
    $scope.currentSlice = [];
    $scope.mode = ['kwic', 'parallel'];
    $scope.sorted = {column: -1, direction: -1};
    $scope.expanded;
    $scope.langNumber;
    $scope.loaded = false;
    $scope.initials_loaded = false;
    $scope.selection;
	$scope.corpusMode = modeKeeper.get ('name');

    var limit = 10;

    $scope.alignText = function (index) {
        return ['right', 'center', 'left'][index];
    }
    processResults.get (true)
        .then (function (response) {

            var lines = response.data.split ("\n");

            $scope.query = lines[0].slice (0, lines[0].length - 2);
            $scope.numRes = lines[1];


			$scope.initials_loaded = true;
        }, function (response) {
            $scope.query = 'error :('
            $scope.numRes = 'error :('
        });
    $scope.results = processResults.get (false)
        .then (function (response) {

                   $scope.results = processResults.prepare (response.data, $scope.corpusMode);
                   $scope.currentSlice = $scope.results.slice (0, $scope.pagination.numPerPage);
                   $scope.expanded = new Array ($scope.pagination.numPerPage).fill (0);
                   $scope.pagination.boundaries = $scope.results.length / $scope.pagination.numPerPage > 5;
                   $scope.langNumber = $scope.getLanguages ().length;
                   $scope.selection = new Array ($scope.results.length).fill (false);
				   $scope.initials_loaded = false;
                   $scope.loaded = true;
                   return response.data;
               }, function (response) {
                   $scope.results = 'error :(' + response;
                   return response;
               }
        );

    $scope.toggleSelection = function (index) {

        var recalculated = ($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage + index;
        $scope.selection[recalculated] = !$scope.selection[recalculated];
    }

    $scope.saveTSV = function (mode) {
        var toSave = [];
        var text = 'Left context\tMatch\tRight Context\t';
        var langs = $scope.getLanguages ();
        var filename = 'results.tsv'
        if (mode === 'all')
            toSave = $scope.results;
        else
        {
            for (var i = 0; i < $scope.selection.length; ++i)
                if ($scope.selection[i] === true)
                    toSave.push ($scope.results[i]);
        }
        for (var i = 1; i < langs.length; ++i) {
            text += langs[i];
            if (i < langs.length - 1)
                text += '\t';
            else
                text += '\n';
        }

        for (var i = 0; i < toSave.length; ++i) {
            var row = toSave[i];


            for (var j = 0; j < row.length; ++j) {

                if (j === 0) {
                    text += processResults.join (row[j][0], false) + '\t';
                    text += processResults.join (row[j][1], false) + '\t';
                    text += processResults.join (row[j][2], false);
                }
                else
                    text += processResults.join (row[j][1], false);
                if (j < row.length - 1)
                    text += '\t';
                else
                    text += '\n';
                }
        }

        var blob = new Blob ([text], {type: 'text/text'});
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            var e = document.createEvent('MouseEvents'),
            a = document.createElement('a');
            a.download = filename;
            a.href = window.URL.createObjectURL(blob);
            a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
            e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
            // window.URL.revokeObjectURL(url); // clean the url.createObjectURL resource
        }
    }

    $scope.$watch ('results', function () {

        if ($scope.results.length > 0)
        {
            $scope.currentSlice = getSlice ();
            $scope.expanded.fill (0);
        }
    });

    $scope.joinLangRows = function (row) {
            var together = row[0].concat (row[1], row[2]);
            return together;
    }

    $scope.limit = function (words, sectionNumber) {
        if (sectionNumber === 1)
            return words;
        else if (sectionNumber === 0) {
            return words.slice (words.length - limit);
        }
        else {
            return words.slice (0, limit);
        }
    }

    $scope.getLanguages = function () {
        return processResults.getLangs ();
    }

    var getSlice = function () {
        var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
        var end = begin + $scope.pagination.numPerPage;
        return $scope.results.slice (begin, end);
    }

    $scope.horizontal = function () {
        var rows = [];
        for (var i = 0; i < $scope.currentSlice.length; ++i) {
			rows.push ($scope.currentSlice[i][0]);
            for (var j = 0; j < $scope.langNumber; ++j) {
                rows.push ($scope.currentSlice[i][j]);

            }
        }

        return rows;
    }

    $scope.showRowKwic = function (index) {
        if (index % ($scope.langNumber + 1) === 0)
            return true;
        return $scope.expanded[Math.floor (index / ($scope.langNumber + 1))];

    }

    $scope.switchView = function () {

        $scope.mode.reverse ();
        // $scope.pagination.numPerPage = $scope.mode[0].perPage;
        // var size = $scope.results.length;
        // var npp = $scope.pagination.numPerPage;
        // var lastPageNumber = Math.floor (size / npp + (size % npp != 0) ? 1 : 0);
        // if ($scope.pagination.currentPage > lastPageNumber)
        //     $scope.pagination.currentPage = lastPageNumber;
        // $scope.currentSlice = getSlice ();
    }



    $scope.pageChanged = function () {
        $scope.currentSlice = getSlice ();
        $scope.expanded.fill (0);
    }

    $scope.showPaginator = function () {
        if ($scope.currentSlice.length === 0 || $scope.pagination.numPerPage >= $scope.results.length) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.expand = function (index) {
        var resNum = Math.floor (index / ($scope.langNumber + 1));
        $scope.expanded[resNum] = Math.pow ($scope.expanded[resNum] - 1, 2);
    }

    var sortResultsLeft = function (a, b) {
        var lena = a[0][0].length;
        var lenb = b[0][0].length;
        if (lenb === 0)
            return 1;
        if (lena === 0)
            return -1;
        if (a[0][0][lena - 1].word > b[0][0][lenb - 1].word)
            return 1;
        else if (a[0][0][lena - 1].word < b[0][0][lenb - 1].word)
            return -1;
        else
            return 0;
    }

    var sortResultsMatch = function (a, b) {
        if (b[0][1].length === 0)
            return 1;
        if (a[0][1].length === 0)
            return -1;
            if (a[0][1][0].word > b[0][1][0].word)
                return 1;
            else if (a[0][1][0].word < b[0][1][0].word)
                return -1;
            else
                return 0;
    }

    var sortResultsRight = function (a, b) {
        if (b[0][2].length === 0)
            return 1;
        if (a[0][2].length === 0)
            return -1;


        if (a[0][2][0].word > b[0][2][0].word)
            return 1;
        else if (a[0][2][0].word < b[0][2][0].word)
            return -1;
        else
            return 0;
    }

    $scope.sortResults = function (section) {

        var tmp = Object.assign ([], $scope.results);
        if (section === 0)
            if ($scope.sorted.column === 0)
            {
                tmp.reverse ();
                $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
            }
            else
            {

                $scope.sorted = {column: 0, direction: 0};
                tmp.sort (sortResultsLeft);

            }
        else if (section === 1)
            if ($scope.sorted.column === 1)
            {
                tmp.reverse ();
                $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
            }
            else
            {
                tmp.sort (sortResultsMatch);
                $scope.sorted = {column: 1, direction: 0};
            }
        else
        {
            if ($scope.sorted.column === 2)
            {
                tmp.reverse ();
                $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
            }
            else
            {
                tmp.sort (sortResultsRight);
                $scope.sorted = {column: 2, direction: 0};
            }
        }

        $scope.results = tmp;
    }

}])

corpus.factory ('processResults', ['$http', 'queryKeeper', 'metaKeeper', function ($http, queryKeeper, metaKeeper) {
    var results_retrieved = false;
    return {
                get: function (onlyInitials) {
                    var queries = {};
                    var langs = queryKeeper.getAlignedLanguages ();
					console.log ('processResults get, qR:', queryKeeper.getQueryRows ());
                    for (var i = 0; i < langs.length; ++i) {
						console.log ('get query for lang ', langs[i].name);
                        queries[langs[i].name] = queryKeeper.getQuery (langs[i]);
                    }
                    var primary = queryKeeper.getPrimaryLanguage ();




                    return $http.post ('backend/get_results.php', {queries: queries, langs: langs, primlang: primary, init: onlyInitials});
                    },
                getLangs: function () {
                    var lang_names = [];
                    var langs = queryKeeper.getAlignedLanguages ();
                    for (var i = 0; i < langs.length; ++i)
                        lang_names.push (langs[i].full_name);

                    return lang_names;
                },
                join: function (words, kwicit) {

                    // take all the words and join them into one line


                    var sticky_left = [',', '.', '?', '!', ':', ';', ')', ']', '%', '$'];
                    var sticky_right = ['(', '['];
                    var space = '';
                    var current = '';
                    var kwic = [];
                    var stick_r = false;
                    var word = '';;
                    for (var i = 0; i < words.length; ++i) {
                        if (words[i]['word'] === '')
                            continue;
                        word = words[i]['word'];
                        if (kwicit && words[i]['matchstart'] === true) {
                            kwic.push (current);
                            current = '';
                        }
                        if (i == 0 || stick_r || (word.length == 1  && sticky_left.indexOf (word) != -1))
                            space = '';
                        else
                            space = ' ';
                        if (word.length == 1 && sticky_right.indexOf (word) != -1)
                            stick_r = true;
                        else
                            stick_r = false;
                        current += space + word;
                        if (kwicit && words[i]['matchend'] === true) {
                            kwic.push (current);
                            current = '';
                        }
                    }
                    kwic.push (current);

                    return kwic;
                },
                prepare: function (data, mode) {
                    var sticky_left = [',', '.', '?', '!', ':', ';', ')', ']', '%', '$'];
                    var sticky_right = ['(', '['];
                    var stick_r = false;
                    var rows = [];

                    var lines = data.split ("\n");
					console.log ('lines:', lines[0]);

                    var row = [];
                    for (var i = 0; i < lines.length; ++i) {
                        // if (i % 1000 === 0)

                        var matching_section = false;

                        var words = [];
                        var flat_words = lines[i].trim().split (' ');
                        var match_beg = 0;
                        var match_end = flat_words.length;
                        for (var j = 0; j < flat_words.length; ++j)
                        {

                            var elements = flat_words[j].split ('/');
                            if (elements.length != 3)
                                continue;
                            var word = {'word': elements[0],
                                        'tag': elements[1],
                                        'lemma': elements[2],
                                       };
                            if (word['word'][0] === '<')
                            {
                                word['word'] = word['word'].slice (1);
                                matching_section = true;
                                match_beg = words.length;
                            }
                            word['match'] = matching_section;
                            if (word['lemma'][word['lemma'].length - 1] == '>')
                            {
                                word['lemma'] = word['lemma'].slice (0, word['lemma'].length - 1);
                                matching_section = false;
                                match_end = words.length;
                            }
                            if (j === 0 || stick_r || (word['word'].length == 1  && sticky_left.indexOf (word['word']) != -1))
                                word['space'] = false;
                            else {
                                word['space'] = true;
                            }
                            if (word['word'].length == 1 && sticky_right.indexOf (word['word']) != -1)
                                stick_r = true;
                            else
                                stick_r = false;
                            // if (matching_section === 'true')

                            words.push (word);
                        }
                        var matchnum;
                        var left = words.slice (0, match_beg);
                        var match = words.slice (match_beg, match_end + 1);
                        var right = words.slice (match_end + 1);



                        if (flat_words[0][0] != '-') {
                            if (row.length > 0)
                                rows.push (row);

                            row = [];
                            matchnum = flat_words[0].slice (0, flat_words[0].length - 1);
                            row.push ([left, match, right]);
                        }
                        else {
                            row.push ([left, match, right]);
                        }
                    }
                    if (row.length === 3 && row[1] != []) {
                        rows.push (row);

                    }
					console.log ('rows:', rows);

                    results_retrieved = true;
                    return rows;
                },
                getResultsRetrieved: function () {
                    return results_retrieved;
                }

    }
}]);

corpus.controller ('frequencyController', ['$scope', '$location', 'settingsKeeper', 'queryKeeper', 'processStatData', 'queryHist', function ($scope, $location, settingsKeeper, queryKeeper, processStatData, queryHist) {
    var settings = settingsKeeper.get ();
    var langs =  queryKeeper.getAlignedLanguages ();
    $scope.results = [];
    $scope.pagination = {
                            currentPage: 1,
                            numPerPage: settings.results.perPage,
                            maxSize: 5,
                            boundaries: false
                        };
    $scope.currentSlice = [];
    $scope.sorted = {column: -1, direction: -1};
    $scope.loaded = false;
    $scope.initials_loaded = false;
    $scope.primary = langs[0];

    $scope.$watch ('results', function () {

        if ($scope.results.length > 0)
        {
            $scope.currentSlice = getSlice ();
            $scope.expanded.fill (0);
        }
    });
    var getSlice = function () {
        var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
        var end = begin + $scope.pagination.numPerPage;
        return $scope.results.slice (begin, end);
    }

    $scope.pageChanged = function () {
        $scope.currentSlice = getSlice ();
        $scope.expanded.fill (0);
    }

    $scope.showPaginator = function () {
        if ($scope.currentSlice.length === 0 || $scope.pagination.numPerPage >= $scope.results.length) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.getQuery = function () {
        return queryKeeper.getQuery ($scope.primary);
    }

    $scope.makeQuery = function (match) {
		console.log ('makeQuery, match:', match)
        var query_tokens = queryKeeper.getAll ($scope.primary);
        var qr = queryKeeper.getQueryRows ();

        queryHist.add (qr, 1);


        for (var i = 0; i < langs.length; ++i) {
            words = match.split ();
			console.log ('coll words, i:', words, i);
            queryKeeper.clear (langs[i], false);
            if (i === 0) {
                for (var j = 0; j < words.length; ++j){
                    word = {token: '', lexeme: '', gramTag: '', caseSensitive: !query_tokens[j].caseSensitive};
                    word[settings.frequency.countBy === 'word' ? 'token' : 'lexeme'] = words[j];
                    queryKeeper.set (langs[i], i, word);
					console.log ('coll qR, i:', queryKeeper.getQueryRows (), i);
                }
            }
        }
        //$location.path ('/results');
    }

    $scope.sortResults = function (col) {
        var tmp = Object.assign ([], $scope.results);
        if ($scope.sorted.column === col)
        {
            tmp.reverse ();
            $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
        }
        else
        {
            $scope.sorted = {column: col, direction: 0};
            tmp.sort ();
        }
        $scope.results = tmp;
    }
    // processStatData.get (true, 'frequency', settings.frequency)
    //     .then (function (response) {

    //         var lines = response.data.split ("\n");
    //         $scope.query = lines[0];
    //         $scope.numRes = lines[1];
	// 		$scope.initials_loaded = true;
    //     }, function (response) {
    //         $scope.query = 'error :('
    //         $scope.numRes = 'error :('
    //     });
    processStatData.get (false, 'frequency', settings.frequency)
        .then (function (response) {
                   var lines = response.data.split ("\n");
                   $scope.query = lines[0];
                   $scope.numRes = lines[1];
                   $scope.results = processStatData.prepare (lines.slice (2));

                   $scope.currentSlice = $scope.results.slice (0, $scope.pagination.numPerPage);
                   $scope.expanded = new Array ($scope.pagination.numPerPage).fill (0);
                   $scope.pagination.boundaries = $scope.results.length / $scope.pagination.numPerPage > 5;
				   $scope.initials_loaded = false;
                   $scope.loaded = true;
                   return response.data;
               }, function (response) {
                   $scope.results = 'error :(' + response;
                   return response;
               }
        );
}]);

corpus.controller ('collocationsController', ['$scope', '$location', 'settingsKeeper', 'queryKeeper', 'processStatData', 'queryHist', function ($scope, $location, settingsKeeper, queryKeeper, processStatData, queryHist) {
    var settings = settingsKeeper.get ();
    var langs =  queryKeeper.getAlignedLanguages ();
	console.log ('COLLO');
    $scope.results = [];
    $scope.pagination = {
                            currentPage: 1,
                            numPerPage: settings.results.perPage,
                            maxSize: 5,
                            boundaries: false
                        };
    $scope.currentSlice = [];
    $scope.sorted = {column: -1, direction: -1};
    $scope.loaded = false;
    $scope.initials_loaded = false;
    $scope.primary = langs[0];
    $scope.ams = [];
    for (var am in settings.collocations.ams) {
        if (settings.collocations.ams[am])
            $scope.ams.push (am);
    }

    $scope.$watch ('results', function () {

        if ($scope.results.length > 0)
        {
            $scope.currentSlice = getSlice ();
            $scope.expanded.fill (0);
        }
    });
    var getSlice = function () {
        var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
        var end = begin + $scope.pagination.numPerPage;
        return $scope.results.slice (begin, end);
    }

    $scope.pageChanged = function () {
        $scope.currentSlice = getSlice ();
        $scope.expanded.fill (0);
    }

    $scope.showPaginator = function () {
        if ($scope.currentSlice.length === 0 || $scope.pagination.numPerPage >= $scope.results.length) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.getQuery = function () {
        return queryKeeper.getQuery ($scope.primary);
    }

    $scope.makeQuery = function (match) {
		console.log ('coll Make query, match', match);
        var query_tokens = queryKeeper.getAll ($scope.primary);
        var qr = queryKeeper.getQueryRows ();

        queryHist.add (qr, 1);

		var tmp1 = jQuery.extend (true, [], queryKeeper.getAll (langs[0]));
		var tmp2 = jQuery.extend (true, [], queryKeeper.getAll (langs[0]));
		console.log ('tmp1 (1):', tmp1);
		console.log ('tmp2 (1):', tmp2, tmp1 == tmp2);
        for (var i = 0; i < langs.length; ++i) {
            queryKeeper.clear (langs[i], false);
			if (i === 0) {
				queryKeeper.set (langs[0], 0, {alternative: tmp1.length + 1});
				var word1 = {token: '', lexeme: '', gramTag: '', caseSensitive: !query_tokens[0].caseSensitive, from: 0, to: settings.collocations.leftContextSize};
				word1[settings.frequency.countBy === 'word' ? 'token' : 'lexeme'] = match;
				queryKeeper.add (langs[0], [word1]);
				queryKeeper.add (langs[0], tmp1);
				word2 = jQuery.extend (true, {}, word1);
				word2.from = '';
				word2.to = '';
				tmp2[tmp2.length - 1].from = 0;
				tmp2[tmp2.length - 1].to = settings.collocations.rightContextSize;
				queryKeeper.add (langs[0], tmp2);
				queryKeeper.add (langs[0], [word2]);
				console.log ('tmp1:', tmp1);
				console.log (tmp2);
				console.log (word1);
				console.log (word2);

			}
        }
		console.log ('coll qR:', queryKeeper.getQueryRows ());
		console.log ('coll query:', queryKeeper.prepareQuery (langs[0]));
        $location.path ('/results');
    }

	var sortByColumn = function (a, b) {	// sorting descending
		if ($scope.sorted.column === 0) {
			if (a[0] > b[0])
				return -1;
			else if (a[0] < b[0])
				return 1;
			else
				return 0;
		}
		return b[$scope.sorted.column] - a[$scope.sorted.column];
	}

    $scope.sortResults = function (col) {
        var tmp = Object.assign ([], $scope.results);

        if ($scope.sorted.column === col)
        {
            tmp.reverse ();
            $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
        }
        else
        {
            $scope.sorted = {column: col, direction: 1};
            tmp.sort (sortByColumn);
        }
        $scope.results = tmp;
    }

    // processStatData.get (true, 'collocations', settings.collocations)
        // .then (function (response) {

            // var lines = response.data.split ("\n");
            // $scope.query = lines[0];
            // $scope.numRes = lines[1];
			// $scope.initials_loaded = true;
        // }, function (response) {
            // $scope.query = 'error :('
            // $scope.numRes = 'error :('
        // });
    processStatData.get (false, 'collocations', settings.collocations)
        .then (function (response) {
                   var lines = response.data.split ("\n");
                   $scope.query = lines[0];
                   $scope.numRes = lines[1];
                   $scope.results = processStatData.prepare (lines.slice (2));
				   $scope.sortResults (5);
                   $scope.currentSlice = $scope.results.slice (0, $scope.pagination.numPerPage);
                   $scope.expanded = new Array ($scope.pagination.numPerPage).fill (0);
                   $scope.pagination.boundaries = $scope.results.length / $scope.pagination.numPerPage > 5;
				   $scope.initials_loaded = false;
                   $scope.loaded = true;
                   return response.data;
               }, function (response) {
                   $scope.results = 'error :(' + response;
                   return response;
               }
        );
}]);

corpus.controller ('ngramsController', ['$scope', '$location', 'settingsKeeper', 'queryKeeper', 'processStatData', 'queryHist', function ($scope, $location, settingsKeeper, queryKeeper, processStatData, queryHist) {
    var settings = settingsKeeper.get ();
    var langs =  queryKeeper.getAlignedLanguages ();
    $scope.results = [];
    $scope.pagination = {
                            currentPage: 1,
                            numPerPage: settings.results.perPage,
                            maxSize: 5,
                            boundaries: false
                        };
    $scope.currentSlice = [];
    $scope.sorted = {column: -1, direction: -1};
    $scope.loaded = false;
    $scope.initials_loaded = false;
    $scope.primary = langs[0];
    $scope.size = settings.ngrams.size;

    $scope.$watch ('results', function () {

        if ($scope.results.length > 0)
        {
            $scope.currentSlice = getSlice ();
            $scope.expanded.fill (0);
        }
    });
    var getSlice = function () {
        var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
        var end = begin + $scope.pagination.numPerPage;
        return $scope.results.slice (begin, end);
    }

    $scope.pageChanged = function () {
        $scope.currentSlice = getSlice ();
        $scope.expanded.fill (0);
    }

    $scope.showPaginator = function () {
        if ($scope.currentSlice.length === 0 || $scope.pagination.numPerPage >= $scope.results.length) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.getQuery = function () {
        return queryKeeper.getQuery ($scope.primary);
    }

    $scope.makeQuery = function (match) {
        var query_tokens = queryKeeper.getAll ($scope.primary);
        var qr = queryKeeper.getQueryRows ();

        queryHist.add (qr, 1);


        for (var i = 0; i < langs.length; ++i) {
            words = match.split (' ');

            queryKeeper.clear (langs[i], false);
            if (i === 0) {
                for (var j = 0; j < words.length; ++j){
                    word = {token: '', lexeme: '', gramTag: '', caseSensitive: !query_tokens[j].caseSensitive};
                    word[settings.frequency.countBy === 'word' ? 'token' : 'lexeme'] = words[j];
                    queryKeeper.set (langs[i], i, word);
                }
            }
        }
        $location.path ('/results');
    }

	var sortByColumn = function (a, b) {
		if ($scope.sorted.column === 0) {
			if (a[0] > b[0])
				return 1;
			else if (a[0] < b[0])
				return -1;
			else
				return 0;
		}
		return a[$scope.sorted.column] - b[$scope.sorted.column];
	}

    $scope.sortResults = function (col) {
        var tmp = Object.assign ([], $scope.results);

        if ($scope.sorted.column === col)
        {
            tmp.reverse ();
            $scope.sorted.direction = Math.pow ($scope.sorted.direction - 1, 2);
        }
        else
        {
            $scope.sorted = {column: col, direction: 0};
            tmp.sort (sortByColumn);
        }
        $scope.results = tmp;
    }

    // processStatData.get (true, 'collocations', settings.collocations)
        // .then (function (response) {

            // var lines = response.data.split ("\n");
            // $scope.query = lines[0];
            // $scope.numRes = lines[1];
			// $scope.initials_loaded = true;
        // }, function (response) {
            // $scope.query = 'error :('
            // $scope.numRes = 'error :('
        // });
    processStatData.get (false, 'ngrams', settings.ngrams)
        .then (function (response) {
                   var lines = response.data.split ("\n");
                   $scope.query = lines[0];
                   $scope.numRes = lines[1];
                   $scope.results = processStatData.prepare (lines.slice (2));

                   $scope.currentSlice = $scope.results.slice (0, $scope.pagination.numPerPage);
                   $scope.expanded = new Array ($scope.pagination.numPerPage).fill (0);
                   $scope.pagination.boundaries = $scope.results.length / $scope.pagination.numPerPage > 5;
				   $scope.initials_loaded = false;
                   $scope.loaded = true;
                   return response.data;
               }, function (response) {
                   $scope.results = 'error :(' + response;
                   return response;
               }
        );
}]);

corpus.factory ('processStatData', ['$http', 'queryKeeper', function ($http, queryKeeper) {
    return {
                get: function (onlyInitials, mode, settings) {
                    var queries = {};
                    var langs = queryKeeper.getAlignedLanguages ();
                    var lnames = [];
                    for (var i = 0; i < langs.length; ++i) {
                        queries[langs[i].name] = queryKeeper.getQuery (langs[i]);
                        lnames.push (langs[i].name);
                    }
                    var primary = queryKeeper.getPrimaryLanguage ();
                    var params =  {queries: queries, langs: lnames, primlang: primary.name, init: onlyInitials};
                    var script = '';
                    if (mode == 'frequency') {
                        script = 'backend/frequency.php';
                        params.countby = settings.countBy;
                    }
                    else if (mode == 'collocations') {
                        script = 'backend/collocations.php';
                        params.collocations = settings;
                    }
                    else if (mode == 'ngrams') {
                        script = 'backend/ngrams.php';
                        params.ngrams = settings;
                    }
                    return $http.post (script, params);
                    },
                prepare: function (lines) {
                    var rows = [];
                    // var lines = data.split ("\n").slice (2);

                    var row = [];
                    for (var i = 0; i < lines.length; ++i) {
                        // if (i % 1000 === 0)

                        var elements = lines[i].split ("\t");
                        for (var j = 1; j < elements.length; ++j)
                            elements[j] = +elements[j];
                        rows.push (elements);
                    }
                    return rows;
                }
    }
}]);

corpus.controller ('settingsController', ['$scope', 'settingsKeeper', function ($scope, settingsKeeper) {
    $scope.languages = settingsKeeper.getLanguages ();
    $scope.switchLang = function (lang) {
        settingsKeeper.setLanguages ($scope.languages);
    }
}]);

corpus.factory ('settingsKeeper', ['$cookies', '$window', 'queryKeeper', function ($cookies, $window, queryKeeper) {

    var settings = {};
    var default_settings =  {
                    open: false,
                    frequency: {'countBy': 'word'},
                    collocations: {'ams': {'llr': true, 'mi': true, 't-score': true, 'z-score': true, 'dice': true, 'mi3': true, 'frequency': true}, 'countBy': 'word', 'leftContextSize': 3, 'rightContextSize': 3, 'threshold': 5},
                    ngrams: {'size': 3, 'countBy': 'word', 'threshold': 2},
                    results: {'perPage': 50, 'mode': 'KWIC', 'limit': 15000}
                };
    return {
        get: function () {
            var fromDisk = $cookies.getObject ('settings');
            if (fromDisk) {
                console.log('cookie served:', Object.keys (fromDisk));
                settings = fromDisk;
            }
            else {
                console.log('loading defaults');
                settings = default_settings;
            }
            // $cookies.remove ('settings');
            return settings;
        },
        set: function (to_set) {
            settings = to_set;
            var now = new $window.Date (),
            exp = new $window.Date (now.getFullYear() +2, now.getMonth(), now.getDate());
            $cookies.putObject ('settings', settings, {expires: exp});
            var cc = $cookies.getObject ('settings');
            console.log ('cookie baked!', cc);
        }

    }
}]);

corpus.directive( 'goClick', ['$location', 'queryKeeper', 'queryHist', function ( $location, queryKeeper, queryHist ) {
  return function ( scope, element, attrs ) {
    var path;
    attrs.$observe( 'goClick', function (val) {
      path = val;
    });

    element.bind( 'click', function () {
        var al  = queryKeeper.getAlignedLanguages ();
        queryHist.add (queryKeeper.getQueryRows());
      scope.$apply( function () {
        $location.path( path );
      });
    });
  };
}]);

corpus.factory ('queryHist', ['$rootScope', function ($rootScope) {
    var history = [];
    var limit = 200;
    var changes = new Array (limit).fill (0);
    return {
        get: function (index, dir) {
            var previous = index - dir;


            if (previous >= changes.length || index >= changes.length){

                return 0;
            }
            if (previous < index) { // moving forward

                return changes[index];
            }
            else {  // moving back

                if (changes[previous] !== 0)
                {

                    for (var i = index; i >= 0; --i){
                        if (changes[i] !== 0)
                            return changes[i];
                    }

                    return 0;
                }
                else {

                    return 0;
                }
            }


            return history[index];
        },
        getAll: function () {
            return history;
        },
        add: function (queryRows, offset = 0) {
            var position = $rootScope.stackPosition + offset;
            if (position > limit)
                changes.push (queryRows);
            else
                changes[position] = queryRows;



            if (history.length > limit){

                history = history.slice (1);
            }
        },
        pop: function () {
            history.pop ();
            changes.pop ();
        }
    }
}]);

corpus.factory ('modeKeeper', function () {
    var mode = {
                name: 'parallel',
                otherMode: 'mono',
                currentCorpusIndex: 0,
                lastMonoIndex: 3,
                corpora: ['parallel', 'au', 'fr', 'uk'],
            };
    return {
        get: function (key) {
            return mode[key];
        },
        getAll: function () {
            return mode;
        },
        getCorpus: function () {
            return mode.corpora[mode.currentCorpusIndex];
        },
        set: function (key, value) {
            mode[key] = value;
            if (key == 'name') {
                if (value == 'parallel') {
                    mode.otherMode = 'mono';
                    mode.currentCorpusIndex = 0;
                }
                else {
                    mode.otherMode = 'parallel';
                    mode.currentCorpusIndex = mode.lastMonoIndex;
                }
            }
        },
        setAll: function (newMode) {
            mode = newMode;
        }
    }
})
