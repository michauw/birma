<?php
include ('../settings/init.php');
$params = json_decode (file_get_contents ('php://input'), true);
$primlang = $params['primlang'];
$queries = $params['queries'];
$collocations = $params['collocations'];
$langs = $params['langs'];
$init = $params['init'];
$primquery = $queries[$primlang];
$secondquery = '';
foreach ($langs as $l) {
	if (($l != $primlang) && $queries[$l])
		$secondquery .= ': ' . $CORPUSNAME[$l] . ' ' . $queries[$l];
}

$collocations = escapeshellarg (json_encode ($collocations));

$params = array ();
$params['cwbdir'] = $CWBDIR;
$params['registry'] = $REGISTRY;
$params['corpusname'] = $CORPUSNAME[$primlang];
$params['primquery'] = $primquery;
$params['secondquery'] = $secondquery;
$params['langs'] = $langs;
$params['primlang'] = $primlang;

$params = escapeshellarg (json_encode ($params));

$command = "python collocations.py $params $collocations";

//var_dump ($collocations);
exec ($command, $out);

echo implode ("\n", $out);
//echo $command;
?>
