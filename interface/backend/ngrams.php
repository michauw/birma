<?php
include ('../settings/init.php');
$params = json_decode (file_get_contents ('php://input'), true);
$primlang = $params['primlang'];
$queries = $params['queries'];
$ngrams = $params['ngrams'];
$langs = $params['langs'];
$init = $params['init'];
$primquery = $queries[$primlang];
$secondquery = '';
foreach ($langs as $l) {
	if (($l != $primlang) && $queries[$l])
		$secondquery .= ': ' . $CORPUSNAME[$l] . ' ' . $queries[$l];
}
$ngrams = escapeshellarg (json_encode ($ngrams));

$params = array ();
$params['cwbdir'] = $CWBDIR;
$params['registry'] = $REGISTRY;
$params['corpusname'] = $CORPUSNAME[$primlang];
$params['primquery'] = $primquery;
$params['secondquery'] = $secondquery;
$params['langs'] = $langs;
$params['primlang'] = $primlang;

$params = escapeshellarg (json_encode ($params));

$command = "python ngrams.py $params $ngrams";

exec ($command, $out);

echo implode ("\n", $out);
?>
