<?php 
 /* created on 16.02.2007 by Roland Meyer
  * revised by Andreas Zeman and Ruprecht von Waldenfels
  * this is the generic result page called by pressing a button on index.php that then loads either the XML-based concordance script, or the CWB HTML based script, or some other
  */
include('settings/init.php'); 
//$acttexts = isset($_POST['acttexts']) ? $_POST['acttexts'] : array('pl','de');	// $acttexts: fullcorpus
$kontextnum = isset($_POST['kontextnum']) ? $_POST['kontextnum'] : 10;			
$kontexttyp = isset($_POST['kontexttyp']) ? $_POST['kontexttyp'] : "word";
if (!isset($_GET['query'])) {
	$langs = isset($_POST['langs']) ? $_POST['langs'] : array();					// $langs: ['pl', 'de']
	$primlang = isset($_POST['primlang']) ? $_POST['primlang'] : array();			// $primlang: 'de'


	foreach ($langs as $x) {
		if ($_POST['query_' . $x]) {
			$query[$x] = $_POST['query_' . $x];
		};
	};		
}
else {
		$primlang="";
		if (isset($_GET['primlang'])) {
			$primlang=$_GET['primlang'];
		if (isset($_GET['langs'])) {
			$langs = explode ('-', $_GET['langs']);
		}
		if (isset($_GET['metaToShow'])) {
			$meta = explode ('-', $_GET['langs']);
		}
	}
	$query[$primlang]=$_GET['query'];
	
}

//$query[$primlang] = '@' . $query[$primlang];
//echo $query[$primlang];

if (isset($_POST['btn']['conc'])){
	include('results_conc.php');
} elseif (isset($_POST['btn']['xml']) || isset($_POST['btn']['xmlfile']) || isset($_GET['query'])) {
	include('results_xml.php');
} elseif (isset($_POST['btn']['pregreplace'])) {
	include('results_xml_pregrep.php');
} elseif (isset($_POST['btn']['frequency'])){
  include('frequency.php');
} elseif (isset($_POST['btn']['collocations'])){
	$collocations = $_POST['collocations'];
	include ('collocations.php'); 
} elseif (isset ($_POST['btn']['ngrams'])){
	  $ngrams = $_POST['ngrams'];
	  include ('ngrams.php');
};
?>
