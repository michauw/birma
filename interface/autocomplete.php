<?php

// Autocomplete funktioniert folgendermassen: In UI(UserInterface) f�ngt beim jeden Tastendr�ck in den Eingabefeldern javascript die eingegeben W�rter ab.
// Relevant ist nur die erste Buchstabe von jedem Wort. Und daruas wird Query generiert, die an Backend bzw. diese Datei geschickt wird.
// So zum Besipel: aus "ich gehe"  wird "i.* g.*". Diese Query wird hierher geschickt und hier wird anfrage an die Suchmschine generiert.
// Query wird nur dann an Backend geschickt, wenn sie ge�ndert wird.
// So zum Beispiel: wen wir aus "ich gehe" "ich ge" machen query wird nicht geschickt, da beides ("i.* g.*"), dagegen "ich gehe essen" wird geschickt, da ("i.* g.* e.*")
// Nach dem die Anfrage an die Suchmaschine unten generiert wurde und an sie geschickt, bekommen wir eine Anwort (gesuchte W�rter) von der Suchmaschine, die wir Parsen und
// mit json an UI schicken, wo es abgefangen wird und in automplete-database geladen. Autocomplete-library ist von jquery

include('settings/init.php');
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
    $CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
    $CQPOPTIONS .= " -b $HARDBOUNDARY";
}


$query = $_GET['query'];
$attribute= $_GET['attribute'];
$lang = $_GET['language'];
$execstring = "$CWBDIR" . "cqpcl -r $REGISTRY" . " '".$CORPUSNAME[$lang]."; A=" . $query . "; count A by ".$attribute.";'";
$outstr = "";
exec($execstring, $outstr);
$out = trim(implode("\n", $outstr));

$stack = array("test");
$i = 0;
$j = 0;

// Extract words/phrases  for autocomplete from out-string of search-engine. Then push this words to array
// and send this array to the UserInterface per json
while ($i < strlen($out) - 10) {
    $word = '';
    if ($out[$i] == "\t") {
        $i++;
        while ($out[$i] != " " || $out[$i + 1] != " ") {
            $word.=$out[$i];
            $i++;
        }
        if (strlen($word) > 0) {
            array_push($stack, trim($word));
        }
    }
    $i++;
}
echo json_encode($stack);
?>
